let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
    'resources/assets/js/bootstrap.js',
    'resources/assets/js/jquery.datetimepicker.full.js',
    'resources/assets/js/fileinput.js',
    'resources/assets/js/slick.js',
    'resources/assets/js/sweetalert2.js',
    'resources/assets/js/front.js'
    ], 'public/js/app.js')
    .styles([
        'resources/assets/css/bootstrap.css',
        'resources/assets/css/jquery.datetimepicker.css',
        'resources/assets/css/font-awesome.css',
        'resources/assets/css/fileinput.css',
        'resources/assets/css/slick.css',
        'resources/assets/css/slick-theme.css',
        'resources/assets/css/sweetalert2.css',
        'resources/assets/css/front.css',
        'resources/assets/css/admin.css'
    ],'public/css/app.css');
