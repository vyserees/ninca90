<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

//FRONT ROUTE

Route::get('/','FrontController@home');
Route::get('/blog', 'FrontController@blog');
Route::post('/blog/newcom', 'FrontController@blogNewcom');
Route::get('/blog/ninca90', 'FrontController@blogNinca90');
Route::get('/blog/{slug}', 'FrontController@blogSingle');
Route::get('/akademija/o-kladjenju', 'FrontController@akademijaOkl');
Route::get('/akademija/sigurica', 'FrontController@akademijaSigurica');
Route::get('/akademija/rizici', 'FrontController@akademijaRizici');
Route::get('/kladionice/soccer', 'FrontController@kladSoccer');
Route::get('/tipovi', 'FrontController@tipovi');
Route::get('/tipovi/dan/{datum}', 'FrontController@tipoviDani');
Route::get('/analize', 'FrontController@analize');
Route::post('/analize/newcom', 'FrontController@analizeNewcom');
Route::get('/analize/filter/sport/{spval}/sort/{soval}', 'FrontController@analizeFilter');
Route::get('/analize/{anal}', 'FrontController@analizeSingle');
Route::get('/moj-profil', 'FrontController@userMojProfil');
Route::get('/moja-podesavanja', 'FrontController@userMojaPodesavanja');
Route::get('/soccer', 'FrontController@soccer');
Route::get('/kontakt', 'FrontController@kontakt');
Route::get('/onama', 'FrontController@onama');
Route::get('/vesti', 'FrontController@vesti');
Route::get('/fantasy', 'FrontController@fantasy');
Route::get('/mvp', 'FrontController@mvp');
Route::get('/uslovi', 'FrontController@uslovi');
Route::get('/pravila', 'FrontController@pravila');
Route::get('/marketing', 'FrontController@marketing');
Route::get('/kolacic', 'FrontController@kolacic');
Route::post('/profil/editpic','FrontController@userEditpic');
Route::post('registracija','FrontController@registracija');
Route::get('registracija/posalji-mejl/{id}','FrontController@posaljiMejl');
Route::get('registracija/potvrdi-mejl','FrontController@potvrdiMejl');
Route::get('registracija/potvrda/{id}/{code}','FrontController@potvrda');




//ADMIN ROUTE

Route::get('/admin', 'HomeController@index');
Route::get('/admin/blog/svi', 'HomeController@blog');
Route::get('/admin/blog/novi', 'HomeController@blogNovi');
Route::post('/admin/blog/new', 'HomeController@blogNew');
Route::get('/admin/blog/izmeni/{id}', 'HomeController@blogIzmeni');
Route::post('/admin/blog/edit','HomeController@blogEdit');
Route::get('/admin/blog/obrisi/{id}','HomeController@blogDel');
Route::get('/admin/blog/komentari', 'HomeController@blogKomentari');
Route::get('/admin/blog/com/acc/{id}', 'HomeController@comAcc');
Route::get('/admin/blog/com/del/{id}', 'HomeController@comDel');
Route::get('/admin/blog/ninca', 'HomeController@blogNinca');
Route::post('/admin/blog/ninca/new', 'HomeController@blogNincanew');
Route::get('/admin/tiketi/svi', 'HomeController@tiketi');
Route::post('/admin/tiketi/add', 'HomeController@tiketiAdd');
Route::get('/admin/tiketi/brisi/{id}', 'HomeController@tiketiDel');
Route::get('/admin/tiketi/na-pocetnu/{id}', 'HomeController@tiketiHome');
Route::get('/admin/analize/sve', 'HomeController@analize');
Route::get('/admin/analize/nova', 'HomeController@analizeNew');
Route::post('/admin/analize/add', 'HomeController@analizeAdd');
Route::post('/admin/analize/result', 'HomeController@analizeResult');
Route::get('/admin/analize/komentari', 'HomeController@analKom');
Route::get('/admin/analize/com/acc/{id}', 'HomeController@acomAcc');
Route::get('/admin/analize/com/del/{id}', 'HomeController@acomDel');
Route::get('/admin/podesavanja/profil', 'HomeController@setProfil');
Route::get('/admin/podesavanja/dodaj-admina', 'HomeController@addAdmin');
Route::get('/admin/podesavanja/sport-klad', 'HomeController@sportKlad');
Route::get('/admin/addadmin/{id}', 'HomeController@adminAdd');
Route::get('/admin/deladmin/{id}', 'HomeController@adminDel');
Route::post('admin/podesavanja/editpic', 'HomeController@adminEditpic');
Route::post('admin/podesavanja/addklad', 'HomeController@addKlad');
Route::post('admin/podesavanja/addsport', 'HomeController@addSport');
Route::get('admin/podesavanja/dellklad/{id}', 'HomeController@dellKlad');
Route::get('admin/podesavanja/dellsport/{id}', 'HomeController@dellSport');





//AJAX RUTE
Route::post('/ajax/gettips','AjaxController@getTips');
Route::post('/ajax/showtik','AjaxController@showTik');
Route::post('/ajax/changeuserdata', 'AjaxController@changeUserData');
