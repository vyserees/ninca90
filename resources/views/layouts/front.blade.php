<!DOCTYPE html>
<html lang="sr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Ninca90</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico">

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet">     <link href="/css/app.css" rel="stylesheet">


    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body>
<div class="social-bar yes-mobile">
    <div class="sb-meni">
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-angle-double-up" title="Vrh stranice"></i></a>
        <a href="https://www.facebook.com/dojcinovicnemanja"><i class="fa fa-facebook" title="Facebook"></i></a>
        <a href="https://twitter.com/Ninca90"><i class="fa fa-twitter" title="Twitter"></i></a>
        <a href=""><i class="fa fa-youtube" title="Youtube"></i></a>
        <a href="javascript:void(0)" id="promo-show" title="Bonus"><i class="fa fa-bell"></i></a>
    </div>
    <div class="sb-promo" style="overflow-y: hidden">
        <button class="btn btn-default btn-xs promo-hide">
            <i class="fa fa-backward"></i>
        </button>
        <a href=""><img src="/images/bonus.png" class="img-responsive"/></a>
    </div>
</div>
<header class="front-header">

   <!-- <div id="header-top">
        "Kladite se mudro!"
    </div> -->

    <div id="header-container">
        <div class="container">
            <div class="row">
                <div class="col-md-2"> <img src="/images/owl.png" alt="logo" class="img-responsive yes-mobile" style="height: 100px"></div>
                <div class="col-md-4">
                    <h1 class="site-title yes-mobile">NINCA 90</h1>
                </div>
                <div class="col-md-6">
                    @if (Route::has('login'))
                            @auth
                            <div class="log-user">
                                <p>Dobrodošli {{\Auth::user()->username}}</p>

                                <ul class="logged-links">

                            @if(\Auth::user()->role==='A')
                                        <li><a href="{{ url('/admin') }}" title="Admin panel"><i class="fa fa-user-secret"></i></a></li>
                                        <!--<li><a href="javascript:void(0)" title="Nova analiza" class="playon playowl"><i class="fa fa-bell"></i></a></li>-->
                                        <audio src="/images/mp3/owl.mp3" id="audio"></audio>
                            @else
                                        <li><a href="{{ url('/moj-profil') }}" title="Moj profil"><i class="fa fa-user"></i></a></li>
                                        <li><a href="{{ url('/moja-podesavanja') }}" title="Moja podešavanja"><i class="fa fa-wrench"></i></a></li>

                                @endif
                                    <li><a href="javascript:void(0)" class="logout-btn"><i class="fa fa-sign-out" title="Odjava"></i></a></li>
                                </ul>
                                <form id="logout-form" action="{{ route('logout') }}" method="post">
                                    {{csrf_field()}}
                                </form>
                            </div>


                            @else
                                <ul class="login-links pull-right">
                                    <li><a href="{{ route('login') }}"><i class="fa fa-sign-in"></i> uloguj se</a></li>
                                    <li><a href="{{ route('register') }}"><i class="fa fa-user-plus"></i> registruj se</a></li>
                                </ul>
                            @endauth
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div id="meni-container-mobile" class="meni-container-mobile">
        <div class="meni-mobile">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="/images/owl1.png" style="width: 30px;"><h1></h1></a>
                        <h1 class="site-title-mobile">NINCA 90</h1>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="/tipovi">Tipovi</a></li>
                        <li><a href="/analize">Analize</a></li>
                        <li class="dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Akademija<i class="fa fa-sort-desc pull-right"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="/akademija/o-kladjenju">O klađenju</a></li>
                                <li><a class="dropdown-item" href="/akademija/sigurica">Sigurica</a></li>
                                <li><a class="dropdown-item" href="/akademija/rizici">Rizici</a></li>
                            </ul>
                        </li>
                        <li><a href="/blog">Blog</a></li>
                        <li class="dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kladionce<i class="fa fa-sort-desc pull-right"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="/akademija/o-kladjenju">Soccer</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <div id="meni-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="meni">
                        <ul>
                            <li><a href="/" style="padding: 15px 20px;"><i class="fa fa-home fa-lg"></i> </a> </li>
                            <li><a href="/tipovi">Tipovi</a></li>
                            <li><a href="/analize">Analize</a></li>
                            <li class="haspodmeni"><a href="javascript:void(0)">Akademija &nbsp;</a>
                            <div class="podmeni">
                                <a href="/akademija/o-kladjenju">O klađenju</a>
                                <a href="/akademija/sigurica">Sigurica</a>
                                <a href="/akademija/rizici">Rizici</a>

                            </div>
                            </li>
                            <li><a href="/blog">Blog</a></li>
                            <li class="haspodmeni"><a href="javascript:void(0)">Kladionice</a>
                                <div class="podmeni">
                                    <a href="/kladionice/soccer">Soccer</a>
                                </div>
                            </li>
                            <li class="pull-right">
                                <div>
                                    <img src="/images/18.png" class="img-responsive">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="container">
    @yield('content')
</section>
<footer class="front-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <ul class="footer-links">
                    <li><a href="/tipovi">Tipovi<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/analize">Analize<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/akademija/o-kladjenju">O klađenju<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/akademija/rizici">Rizici<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/akademija/sigurica">Sigurica<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/blog">Blog<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/kladionice/soccer">Soccer kladionica<i class="fa fa-angle-right"></i></a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="footer-links">
                    <li><a href="/onama">O nama<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/kontakt">Kontakt<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/vesti">Vesti<i class="fa fa-angle-right"></i></a></li>
<!--                    <li><a href="https://fantasy.premierleague.com/" target="_blank">Fantasy Football<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="http://www.mvp.rs/mvp-fantasy" target="_blank">MVP<i class="fa fa-angle-right"></i></a></li>   -->
                    <li><a href="/uslovi">Uslovi korišćenja<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/pravila">Pravila tipovanja<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/marketing">Marketing<i class="fa fa-angle-right"></i></a></li>
                    <li><a href="/kolacic">Korišćenje kolačića<i class="fa fa-angle-right"></i></a></li>
                </ul>
            </div>
            <div class="col-md-3" style="text-align: center">
                <ul class="social">
                    <li><a href=""><i class="fa fa-facebook"></i></a> </li>
                    <li><a href=""><i class="fa fa-twitter"></i></a> </li>
                    <li><a href=""><i class="fa fa-youtube"></i></a> </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="footer-trade">
    <p>Copyright </p>
</div>
<script src="/js/app.js"></script>
</body>
</html>