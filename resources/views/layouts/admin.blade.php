<!DOCTYPE html>
<html lang="sr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Ninca90</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico">

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet">     <link href="/css/app.css" rel="stylesheet">

    <!-- include summernote css/js-->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body>

<header class="adm-header">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <ul class="adm-meni">
                    <li><a href="/">ninca90.com</a></li>
                    <li><a href="/admin">Home</a> </li>
                    <li class="haspodmeni">
                        <a href="javascript:void(0)">Blog<span class="pull-right"><i class="fa fa-chevron-down"></i></span></a>
                        <ul class="podmeni adm-podmeni">
                            <li><a href="/admin/blog/svi">Svi blogovi</a></li>
                            <li><a href="/admin/blog/novi">Dodaj novi blog</a></li>
                            <li><a href="/admin/blog/komentari">Komentari blogova</a></li>
                        </ul>
                    </li>
                    <li><a href="/admin/tiketi/svi">Tiketi</a> </li>
                    <li class="haspodmeni">
                        <a href="javascript:void(0)">Analize<span class="pull-right"><i class="fa fa-chevron-down"></i></span></a>
                        <ul class="podmeni adm-podmeni">
                            <li><a href="/admin/analize/sve">Sve analize</a></li>
                            <li><a href="/admin/analize/nova">Dodaj novu analizu</a></li>
                            <li><a href="/admin/analize/komentari">Komentari analiza</a></li>
                            <li><a href="/admin/analize/statistika">Statistika analiza</a></li>
                        </ul>
                    </li>
                    <li class="haspodmeni">
                        <a href="javascript:void(0)">Podešavanja<span class="pull-right"><i class="fa fa-chevron-down"></i></span></a>
                        <ul class="podmeni adm-podmeni">
                            <li><a href="/admin/podesavanja/profil">Profil</a></li>
                            <li><a href="/admin/podesavanja/dodaj-admina">Dadaj admina</a></li>
                            <li><a href="/admin/podesavanja/sport-klad">Sportovi i kladionice</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="adm-profil">
                    <p>Dobrodošao {{\Auth::user()->firstname.' '. \Auth::user()->lastname}}</p>
                    <form action="{{ route('logout') }}" method="post">
                        {{csrf_field()}}
                        <button class="btn btn-danger btn-xs" title="Izlogujte se"><i class="fa fa-sign-out fa-lg"></i></button>
                    </form>
                </div>

            </div>
        </div>
    </div>

</header>

<section class="container">
    @yield('content')
</section>
<footer style="margin-top: 40px"></footer>
<script src="/js/app.js"></script>
<script src="https://use.fontawesome.com/93a35442f7.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
</body>
</html>