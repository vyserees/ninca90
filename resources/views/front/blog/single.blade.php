@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Kladioničarski blog</h1>
        </div>
        <div class="col-md-9">
            <div class="blog-single">
                <div class="blog-single-img">
                    <h2 class="blog-single-title">{{$blog->title}}</h2>
                    <img src="/images/avatars/{{\App\User::find($blog->user_id)->avatar}}" alt="Slika avatara" class="blog-single-autor" title="{{\App\User::find($blog->user_id)->username}}">
                    <div class="blog-single-vreme">
                        <i class="fa fa-calendar-o"></i>
                        <strong>{{date('d/m',strtotime($blog->created_at))}}</strong>
                    </div>
                    <img src="/images/blog/{{$blog->image}}" class="img-responsive">
                </div>
                <article class="blog-single-tekst">
                    {!! $blog->content !!}
                </article>
                <div class="koment">
                    <h2 class="blog-single-list-title">KOMENTARI</h2>
                    @if(strlen(session('new-blog-com-alert'))>0)
                        <h3 class="alert alert-warning"><i class="fa fa-warning"></i> {{session('new-blog-com-alert')}}</h3>
                        <?php session(['new-blog-com-alert'=>'']);?>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            @guest
                                <h3 class="alert alert-danger">
                                    <i class="fa fa-warning"></i> Morate biti ulogovani da bi postavili komentar
                                </h3>
                                @else
                            <form action="/blog/newcom" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="url" value="{{Request::path()}}">
                                <input type="hidden" name="aid" value="{{$blog->id}}">
                                <textarea name="text" class="form-control" rows="5" placeholder="unesite tekst komentara..." required></textarea>
                                <button type="submit" class="pull-right btn btn-primary btn-lg">Postavi komentar</button>
                            </form>
                                    @endguest
                        </div>
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-12">
                            @if(count($coms)>0)
                                @foreach($coms as $com)
                                    <div class="koment-users">
                                        <div class="koment-title">
                                            <img src="/images/avatars/{{$com->users->avatar}}">
                                            {{$com->users->username}}
                                            <span class="pull-right">{{date('H:i - d.m.Y.',strtotime($com->created_at))}}</span>
                                        </div>
                                        <article class="koment-text">
                                            {!! $com->text !!}
                                        </article>
                                    </div>
                                @endforeach
                                {{$coms->links()}}
                            @else
                                <p class="blog-single-nocom">Budite prvi koji ćete postaviti komentar</p>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <h3 class="blog-single-list-title">Ostali blogovi</h3>
            @foreach($blogs as $blog)
                <a href="/blog/{{$blog->slug}}">
                    <div class="blog-single-list-lay">
                        <div class="blog-single-list">
                            <img src="/images/blog/{{$blog->image}}" class="img-responsive">
                            <article>
                                <p class="blog-subtitle"> <i class="fa fa-user"></i> {{\App\User::find($blog->user_id)->firstname.' '.\App\User::find($blog->user_id)->lastname}}
                                    <span class="pull-right"><i class="fa fa-calendar"></i> {{date('d.m.Y.',strtotime($blog->created_at))}}</span>
                                </p>
                                <h3>{{$blog->title}}</h3>
                            </article>
                        </div>
                    </div>
                </a>
            @endforeach

        </div>
    </div>
@stop