@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-title">Ninca90 - Blog</h1>
                </div>
                <div class="col-md-12">
                    <?php $imgs = explode(';',$nin->image);?>
                    <a href="/blog/ninca90">
                        <div class="blog-ninca">
                            <div class="blog-owl">
                                <?php foreach($imgs as $img){?>
                                <div style="height: 450px; overflow-y:hide;">
                                    <img src="/images/blog/{{$img}}" class="img-responsive">
                                </div>
                                <?php }?>
                            </div>
                            <div class="blog-ninca-title">
                                <h1>NAJBOLJI TIPSTER - Ninca90</h1>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @foreach($blogs as $blog)
            <div class="row blog-part blog-part-min">
                <div class="col-md-4">
                    <a href="/blog/{{$blog->slug}}">
                        <img src="/images/blog/{{$blog->image}}" class="img-responsive">
                    </a>
                </div>
                <div class="col-md-8">
                    <h1 class="blog-title-min"><a href="/blog/{{$blog->slug}}">{{$blog->title}}</a></h1>
                    <p class="blog-subtitle">Autor : {{\App\User::find($blog->user_id)->firstname.' '.\App\User::find($blog->user_id)->lastname}}
                        <span class="pull-right">Datum : {{date('d.m.Y.',strtotime($blog->created_at))}}</span>
                    </p>
                    <article class="blog-text">
                        {{$blog->excerpt}}
                    </article>
                </div>
            </div>
            @endforeach
        </div>

    </div>
@stop