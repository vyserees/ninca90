@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Ninca90 - NAJBOLJI TIPSTER NA SVETU</h1>
        </div>
        <div class="col-md-9">
            <div class="ninca-prica row">
                <div class="col-md-12">
                    <p class="ninca-prica-bold">Pozdrav svim ljubiteljima sporta i klađenja. Moje ime je Nemanja, u svetu tipovanja poznatiji sam kao
                        Ninca90. Dvogodišnji sam uzastopni pobednik na renomiranom tipsterskom sajtu <a href="https://www.bettingexpert.com/">Betting Expert</a> i to
                        2016. i 2017. godine. Put do tog dostignuća bio je više nego buran i zanimljiv. Da bih do tog
                        ostvarenja došao morao sam da prevalim dugu distancu od kladioca rekreativca do nezvanično
                        <a href="/blog/ninca90">NAJBOLJEG TIPSTERA NA SVETU</a>.
                    </p>

                    <img src="/images/blog/pehar.jpeg" class="img-responsive">
                    <blockquote>
                        <p>Treba razgraničiti šta je kladilac, a šta tipster.</p>
                        <p>Kladilac je onaj koji se kladi za određenu svotu novca kako bi napravio profit od tog klađenja i time
                            svesno ulazi u rizik da taj novac izgubi.</p>
                        <p>Tipster je onaj koji daje predloge za klađenje drugima, na koje može sam da se kladi, ali i ne mora.
                            Zavisi od okolnosti(u daljem tekstu objašnjenje)</p>
                    </blockquote>
                </div>
                <div class="col-md-12">
                    <p>Moj početak u vezi sa svim ovim vezan je za praćenje sporta. Ne mogu ni po kojem osnovu da tvrdim
                        da neko ko ne prati sport neće imati uspeha sa sportskim klađenjem. Te dve stvari su što se tiče
                        mene usko povezane, ali i na jedan vrlo čudan način sasvim posebne. Naravno, sjajno je, i dodatni
                        plus ako se dobro razumete u pojedine sportove, lige, timove i sl. ali to nije i presudno u mojoj praksi.
                        Detaljnije o tome možete pogledati u temi <a href="/akademija/o-kladjenju">o klađenju.</a>
                    </p>
                </div>
                <div class="col-md-12"><hr></div>
                <div class="col-md-6">
                    <p>Sport je zabava za milione ljudi, za neke je i više od toga. Pored besplatne zabave i gledanja
                        sportskih mečeva uživo i preko TV-a, ja sam jednu drugu vrstu zabave plaćao. Klađenje na ishode
                        sportskih događaja. Ta zabava je kompletno maskirala onu prvu jer je donosila mnogostruko veću
                        dozu adrenalina. To je bilo jednostavno klađenje bez bilo kakve strategije i rezona. Klađenje na
                        sopstvene favorite, ponekad čak i nasumice. Uzimanje raznih dojava, nameštenih mečeva, <a href="/akademija/sigurica">sigurica</a>
                        po internetu. Kada nekog kladioca pitate kako stoji sa novcem recimo zadnjih godinu-dve dobićete
                        odgovor "Tu sam negde" ili još gore "Ko zna koliko sam pukao", a nastaviti da se kladi po ustaljenom
                        režimu.</p>

                    <img src="/images/blog/kockar.jpeg" class="img-responsive">
                    <p>Dakle, vođenje računa(banke) je prva stavka koja se mora prihvatiti u cilju dokazivanja tvrdnje da smo
                        u plusu na kladionici ili nekoj "pozitivnoj nuli".</p>
                </div>

                <div class="col-md-6">
                    <p> Ne samo zbog pukog dokazivanja, već i praćenja
                        celokupnog bilansa(profita). Posle 5 godina intenzivnog klađenja rešio sam da počnem sa mesečnim
                        preračunavanjem i nakon samo par meseci uvideo da ću po šablonu i načinu na koji se kladim samo
                        nastaviti da srljam u debeli minus. Takva "zabava" mi se više nije svidela... Napravio sam pauzu i
                        počeo da se bavim drugim aktivnostima.</p>
                    <p>Pošto su mi i dalje bile primarne web stranice sa sportskim sadržajem nekako sam naleteo na Betting
                        Expert, sajt koji nudi besplatne tipove i priređuje takmičenje u tipovanju konačnih rezultata sportskih
                        događaja sa vrlo pristojnim nagradnim fondom. Primarni cilj tog sajta je da ponudi prognoze i analize
                        sportskih događaja kao svojevrsnu pomoć. Ipak treba se opredeliti za prave tipstere, a to je tamo vrlo
                        lako jer je statistika profita svih tipstera na sajtu vrlo transparentna.</p>

                    <img src="/images/blog/be.jpg" class="img-responsive">
                </div>

                <div class="col-md-12"><hr></div>
                <div class="col-md-6">
                    <p>Prvih 7-8 meseci kao tipstera početnika bilo je za mene vrlo neuspešno. Usvojio sam loše navike iz
                        dana klađenja i sada mogao tačno da vidim na svojoj tabeli profita koliko bi me to koštalo da sam se
                        stvarno kladio. Još uvek nisam mogao da pomognem sebi, a kamoli drugima svojim tipovima. Ipak,
                        sve to je imalo i dobre strane. Malo po malo počinjao sam da shvatam gde grešim. Jedan od ključnih
                        nalaza je bio onaj da dobra procena konačnih ishoda događaja i nema neki veliki značaj, pa bila ona i
                        visokoprocentna, ako kvote ne budu imale VALUE(vrednost). Kraće rečeno, kvote diktiraju celu igru.
                        Tako sam počeo da se zanimam za Teoriju verovatnoće.</p>

                    <img src="/images/blog/kvote.jpeg" class="img-responsive">
                    <p>Nekih mesec dana je trajao taj prelazni period od konstantnog gubitnika do kladioca/tipstera koji je
                        dugoročno gledano uvek u plusu. Mnogo stvari od strpljenja pa do discipline u klađenju je trebalo doterati.
                        Dosta se redova literature moralo pročitati... Ne treba posebno naglašavati da sam u međuvremenu ponovo
                        počeo da se kladim i to sa taktikom kojom i tipujem rezultate na Betting Expert takmičenju, ali i još par
                        sajtova pride.</p>
                </div>
                <div class="col-md-6">
                    <p> Novac je konačno počeo da dolazi, samim tim počela su da se zatvaraju vrata kladionica.
                        Online, ali i onih lokalnih. Novčani limiti, spuštanje kvota, pa čak i restrikcije postale su uobičajen
                        obrazac ponašanja kladionica u ovim situacijama. Kladionice smatraju da niste isplativi za njihovo tržište
                        ako ste dobitan igrač, njima trebaju igrači koji igraju iz zabave i koje ne zanima mnogo novčana korist.
                        Oni igrači koji se oslanjaju na sujeverja i neke druge više sile. Problem je što dosta njih ima i problem sa
                        impulsivno kompulzivnim poremećajem kockanja(<a href="/akademija/rizici">rizici</a>) Kladionice ove igrače
                        retko kad isključuju iz igre. Zašto bi kad im donose najveći profit. Pribegavanje raznim metodama kako bi
                        se zaobišli limiti i restrikcije kladionica naravno postoje. Ulozi na jedan par ne smeju da budu visoki i
                        potrebno je naći što vise value opklada. Ipak posle nekog vremena konstantnog dobijanja kladionice će vas
                        jednostavno ugasiti. Dupli nalozi su kasnije jedino rešenje... Svako ko je uporan i snalažljiv uspeće da
                        odigra svoj value bet. Ipak, jedno je sigurno. Svakim zarađenim dinarom postaje sve napornije doći do novog.</p>

                    <img src="/images/blog/lova.jpeg" class="img-responsive">
                </div>
                <div class="col-md-12"><hr></div>
                <div class="col-md-6">
                    <p>Mesecima je moj novčani bilans rastao ali i rejting u svetu tipovanja i klađenja. Posle par mesečnih
                        pobeda na Betting Expert takmičenjima uspeo sam da uđem u konkurenciju za godišnji specijal
                        Tipster of the Year i posle velike borbe do poslednjih dana juna, pobedio Zamija dvogodišnjeg
                        uzastopnog prvaka pre mene. To mi je donelo <b>10.000 €</b> i posetu <b><i>Kopenhagenu</i></b> i kancelariji firme
                        na par dana.</p>
                    <img src="/images/blog/table.jpeg" class="img-responsive">
                    <img src="/images/blog/cek.jpeg" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <p>Slično se ponovilo i u narednoj sezoni. Ponovo je bila mrtva trka do poslednjeg meseca. Sa maltene
                        duplo većim profitom i procentom uspešnosti uspeo sam da odbranim titulu, koja je sada promenila
                        naziv u World tipster champion. Ista nagradna suma i isti put za glavni grad Danske. Ponovni susret
                        sa glavnim ljudima u firmi <a href="https://bettercollective.com/">Better Collective</a> koja poseduje već spomenuti sajt i ima vodeću ulogu na
                        globalnom nivou što se tiče transparentnosti u industriji klađenja. Ovog puta bilo je svakakvih
                        aktivnosti, jedna od njih je bila i intervju za Youtube kanal.</p>

                    <img src="/images/blog/torta.jpeg" class="img-responsive">
                </div>

                <div class="col-md-12">
                    <h2>Intervju za Youtube kanal</h2>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/AVN9xpeClKY" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-12">
                    <p>Ovaj sajt bi trebalo da bude pomoć svima koji se klade, neka smernica onima koji se klade samo iz
                        zabave, ali i onima koji žele malo dublje da uđu u materiju. Pokušaću da sa svojim timom ljudi koje
                        sam skupio prenesem znanje opširnom akademijom i kvalitetnim analizama.</p>
                    <p style="font-size: 2em;"><b>DOBRODOŠLI I PROBAJMO DA MAKAR MALO SMANJIMO PROFIT KLADIONICAMA!</b></p>
                </div>

        </div>
    </div>
        <div class="col-md-3">
            @include('front.sidebar')
        </div>
@stop