@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12"><p>&nbsp;</p></div>
        <div class="col-md-8">
            <div class="home-tiket-layout">
                <h3>TIKET DANA</h3>
                <div class="tiket home-tiket">

                    <article class="tiket-tip">
                        <table class="table tiket-table">
                            <tr>
                                <th style="text-align: center;" width="7%"></th>
                                <th style="text-align: center;" width="10%">Datum</th>
                                <th style="text-align: center;" width="10">Vreme</th>
                                <th>Par</th>
                                <th style="text-align: center;" width="10%">Tip</th>
                                <th style="text-align: center;" width="10%">Kvota</th>
                            </tr>
                            @foreach($tik->ticketdetails as $td)
                                <tr>
                                    <td style="text-align: center;"><img class="img-responsive" src="/images/sports/{{$td->sporticon->icon}}"/></td>
                                    <td style="text-align: center;">{{date('d.m.',strtotime($td->datum))}}</td>
                                    <td style="text-align: center;">{{date('H:i',strtotime($td->vreme))}}</td>
                                    <td>{{$td->par_home}} - {{$td->par_away}}</td>
                                    <td style="text-align: center;">{{$td->tip}}</td>
                                    <td style="text-align: center;">{{$td->kvota}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </article>
                        <article class="tiket-uplata">
                            <div class="home-tiket-klad">
                                <a href="{{\App\Location::find($tik->kladionica)->url}}"><img src="/images/kladionice/{{\App\Location::find($tik->kladionica)->icon}}" alt="Slika kladionioce" style="height:50px;"/></a>
                            </div>
                            <p style="text-align: right;">Ukupna kvota: {{$tik->kvota}}<br>Ulog: {{$tik->verovatnoca}}/10<br>Dobitak: {{$tik->verovatnoca*$tik->kvota}}</p>
                            <div class="tiket-worning">
                                <p>Tipove uzimate na sopstvenu odgovornost. Utakmice nisu 100% sigurne.</p>
                            </div>
                            <a href="/tipovi" class="btn btn-primary btn-sm home-tiket-button">Pogledajte ostale tikete</a>
                        </article>

                </div>

            </div>
            <div class="home-owl">
                <?php foreach($tips as $tip){?>
                    <div class="tips-layout home-tips-layout">
                        <div class="tips-datum">
                            <i class="fa fa-calendar"></i> {{date('d.m.Y.',strtotime($tip->datum))}} <i class="fa fa-clock-o"></i> {{date('H:i',strtotime($tip->datum))}}
                        </div>
                        <div class="tips-klad">
                            <a href="/analize/{{$tip->slug}}" class="btn btn-primary">Pogledajte analizu</a>
                        </div>
                        <div class="tips-profil">
                            <span class="tips-profil-img" title="{{$tip->users->username}}"><a href=""><img src="/images/avatars/{{$tip->users->avatar}}" alt="Slika korisnika" class="img-responsive"></a></span>
                            <span class="tips-yield">{{$tip->users->stats->procenat}}%</span>
                        </div>
                        <div class="tips-par home-tips-par">
                            <a href="/analize/{{$tip->slug}}">
                                <img src="/images/sports/{{$tip->sports()->first()->icon}}" alt="{{$tip->sports()->first()->name}}" title="{{$tip->sports()->first()->name}}">
                                <span>{{$tip->pars->home.' - '.$tip->pars->away}}</span>
                            </a>
                        </div>
                    </div>
                <?php }?>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <img src="/images/be-nin.jpg" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <div class="home-stat">
                        <h3>Statistika Ninca90</h3>
                        <ul>
                            <li>Broj tipova<em class="pull-right">{{$stat->broj}}</em></li>
                            <li>Učinak (W-L-V)<em class="pull-right">{{$ucinak}}</em></li>
                            <li>Ulog<em class="pull-right">{{$stat->ulog}}</em></li>
                            <li>Profit<em class="pull-right">{{$stat->profit}}</em></li>
                            <li>Uspešnost<em class="pull-right">{{$stat->procenat}}%</em></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="nin-prica">
                <img src="/images/nt.jpeg" class="img-responsive">
                <h3><a href="/blog/ninca90">NINCA90 - NAJBOLJI TIPSTER NA SVETU</a></h3>
                <p>Pozdrav svim ljubiteljima sporta i klađenja. Moje ime je Nemanja, u svetu tipovanja poznatiji sam kao
                    Ninca90. Dvogodišnji sam uzastopni pobednik na renomiranom tipsterskom sajtu <a href="https://www.bettingexpert.com/user/profile/ninca90">Betting Expert</a> i to
                    2016. i 2017. godine. Put do tog dostignuća bio je više nego buran i zanimljiv. <a href="/blog/ninca90">Pročitajte više...</a></p>
            </div>
            <div class="sidebar-img">
                <a href=""><img src="/images/logo1.png" class="img-responsive"/></a>
            </div>
            <div class="sidebar-img">
                <a href=""><img src="/images/be.jpg" class="img-responsive"/></a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="home-akad-layout">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="home-akad-title"><i class="fa fa-mortar-board"></i> Akademija</h2>
                    </div>
                    <div class="col-md-4">
                        <div class="home-akad">
                            <i class="fa fa-book"></i>
                            <a href="/akademija/o-kladjenju"><h2>O klađenju</h2></a>
                            <p>Praesent imperdiet elit vitae enim convallis, semper efficitur mi cursus. Nulla facilisi. Proin mattis, odio vel vulputate fringilla, eros nibh porta quam, sed viverra velit libero sed velit. Nam pellentesque erat tristique, lacinia ipsum eu, rutrum massa. Sed rhoncus, purus ac lobortis aliquet, sem neque maximus est, nec vestibulum metus quam eu augue.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="home-akad">
                            <i class="fa fa-thumbs-o-up"></i>
                            <a href="/akademija/sigurica"><h2>Sigurica</h2></a>
                            <p>Nunc sed felis fringilla ante hendrerit luctus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet bibendum odio. Aliquam ultrices massa eu turpis venenatis, et fringilla elit lacinia. Morbi hendrerit tincidunt cursus.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="home-akad">
                            <i class="fa fa-percent"></i>
                            <a href="/akademija/rizici"><h2>Rizici</h2></a>
                            <p>Vestibulum iaculis dignissim urna. Duis id libero nec eros efficitur luctus eu sed ipsum. Nulla gravida ut orci in vehicula. Cras lorem lorem, mattis vel elit vitae, sodales sodales orci. In rutrum vehicula mi, a porttitor risus efficitur quis.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="home-blog-layout">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="home-blog-title"><i class="fa fa-newspaper-o"></i> Najnoviji blogovi</h2>
                            @foreach($blogs as $blog)
                                <div class="col-md-4">
                                    <a href="/blog/{{$blog->slug}}">
                                        <div class="blog-single-list-lay">
                                            <div class="blog-single-list">
                                                <img src="/images/blog/{{$blog->image}}" class="img-responsive">
                                                <article>
                                                    <p class="blog-subtitle"> <i class="fa fa-user"></i> {{\App\User::find($blog->user_id)->firstname.' '.\App\User::find($blog->user_id)->lastname}}
                                                        <span class="pull-right"><i class="fa fa-calendar"></i> {{date('d.m.Y.',strtotime($blog->created_at))}}</span>
                                                    </p>
                                                    <h3>{{$blog->title}}</h3>
                                                </article>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
    </div>
@stop