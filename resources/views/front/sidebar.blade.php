<div>
    <div class="sidebar-img">
        <a href=""><img src="/images/logo1.png" class="img-responsive"/></a>
    </div>
    <div class="sidebar-img">
        <a href=""><img src="/images/be.jpg" class="img-responsive"/></a>
    </div>
    <h3 class="blog-single-list-title">Najnovije priče</h3>
    @foreach(\App\Article::orderBy('created_at', 'desc')->limit(3)->get() as $blog)
        <a href="/blog/{{$blog->slug}}">
            <div class="blog-single-list-lay">
                <div class="blog-single-list">
                    <img src="/images/blog/{{$blog->image}}" class="img-responsive">
                    <article>
                        <p class="blog-subtitle"> <i class="fa fa-user"></i> {{\App\User::find($blog->user_id)->firstname.' '.\App\User::find($blog->user_id)->lastname}}
                            <span class="pull-right"><i class="fa fa-calendar"></i> {{date('d.m.Y.',strtotime($blog->created_at))}}</span>
                        </p>
                        <h3>{{$blog->title}}</h3>
                    </article>
                </div>
            </div>
        </a>
    @endforeach
    <!--
    <div class="sidebar-img">
        <a href=""><img src="/images/pinn.jpg" class="img-responsive"/></a>
    </div>
    <div class="sidebar-img">
        <a href=""><img src="/images/logo3.png" class="img-responsive"/></a>
    </div>
    <div class="sidebar-img">
        <a href=""><img src="/images/logo4.png" class="img-responsive"/></a>
    </div>
    -->
</div>