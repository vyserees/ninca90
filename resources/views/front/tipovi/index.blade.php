@extends('layouts.front')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h1 class="page-title">TIPOVI</h1>
    </div>
    <div class="col-md-9">
        <div class="tiketi-dani">
                    @foreach($days as $key=>$value)
                        <a href="/tipovi/dan/{{$value}}" class="btn btn-primary @if($value === $datum) tiket-dan-active @endif"><i class="fa fa-calendar"></i> {{$key}}</a>
                    @endforeach

        </div>
        @foreach($tiks as $tik)
            <?php switch ($tik->tip){
                case '1':
                    $title='Singl';
                    break;
                case '2':
                    $title='Dubl';
                    break;
                default:
                    $title='tiket';
            } ?>
        <div class="tiket">
            <div class="tiket-baner">
                <img class="img-responsive" src="/images/hot1.png">
                <p>{{$tik->vrsta}}</p>
            </div>
            <div class="tiket-vreme">
                <p>Tiket kreiran:</p>
                <p><i class="fa fa-calendar"></i> {{date('d.m.Y.', strtotime($tik->created_at))}}</p>
                <p><i class="fa fa-clock-o"></i> {{date('H:i:s', strtotime($tik->created_at))}}</p>
            </div>
            <h1 class="tiket-3">{{$title}}</h1>
            <article class="tiket-tip">
                <table class="table tiket-table">
                    <tr>
                        <th style="text-align: center;" width="5%"></th>
                        <th style="text-align: center;" width="10%">Datum</th>
                        <th style="text-align: center;" width="10">Vreme</th>
                        <th>Par</th>
                        <th style="text-align: center;" width="10%">Tip</th>
                        <th style="text-align: center;" width="10%">Kvota</th>
                    </tr>
                    @foreach($tik->ticketdetails as $td)
                    <tr>
                        <td style="text-align: center;"><img class="img-responsive" src="/images/sports/{{$td->sporticon->icon}}"/></td>
                        <td style="text-align: center;">{{date('d.m.',strtotime($td->datum))}}</td>
                        <td style="text-align: center;">{{date('H:i',strtotime($td->vreme))}}</td>
                        <td>{{$td->par_home}} - {{$td->par_away}}</td>
                        <td style="text-align: center;">{{$td->tip}}</td>
                        <td style="text-align: center;">{{$td->kvota}}</td>
                    </tr>
                        @endforeach
                </table>
            </article>
            <hr style="border-color: #49274a;">
            <article class="tiket-uplata">
                <p><a href="{{\App\Location::find($tik->kladionica)->url}}"><img src="/images/kladionice/{{\App\Location::find($tik->kladionica)->icon}}" alt="Slika kladionioce" style="height:50px;"/></a> <span class="pull-right">Ukupna kvota: {{$tik->kvota}}<br>Ulog: {{$tik->verovatnoca}}/10<br>Dobitak: {{$tik->verovatnoca*$tik->kvota}}
	    			</span></p>
                <div class="tiket-worning">
                    <p>Tipove uzimate na sopstvenu odgovornost. Utakmice nisu 100% sigurne.</p>
                </div>
            </article>
        </div>
        @endforeach
    </div>
    <div class="col-md-3">
        @include('front.sidebar')
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
</body>
</html>



@endsection