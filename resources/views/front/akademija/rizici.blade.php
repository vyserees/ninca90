@extends('layouts.front')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <h1 class="page-title">Rizici</h1>
            <h3>Quisque vel maximus dolor</h3>
            <p>Proin ligula est, feugiat at enim eu, placerat ornare nulla. Cras rutrum quam in mauris ullamcorper egestas. Vivamus congue tincidunt interdum. Curabitur ligula ex, rutrum ut nisi vitae, mattis sagittis quam. Phasellus non iaculis nibh. Vestibulum id purus lorem. Proin tempus massa at auctor volutpat.</p>
            <div class="rizik">
                <img src="/images/player.jpg" class="img-responsive">
            </div>
            <p>Nulla purus justo, molestie at sem non, efficitur fermentum odio. Nulla vulputate faucibus sollicitudin. Etiam ullamcorper nulla ut nulla tristique aliquam eu eget lectus. Sed sed nisi quis turpis hendrerit viverra. Nam placerat nibh eget faucibus porta. Proin in libero erat. Ut luctus quam vel est posuere, at molestie nisl convallis. Sed condimentum lectus eu lorem dictum, vitae convallis nisi eleifend. Donec volutpat fermentum nulla, eu lobortis diam placerat vitae. Nam felis odio, bibendum quis mauris a, blandit posuere est. Integer malesuada nisi eu tristique pretium. Suspendisse potenti. Praesent ac odio sed diam ullamcorper congue. Phasellus tincidunt risus ut ante rutrum malesuada.</p>
            <div class="rizik">
                <img src="/images/bl.jpg" class="img-responsive">
            </div>
            <h3>Maecenas rhoncus fringilla egestas</h3>
            <p>Sed eleifend purus faucibus nibh dignissim, sit amet imperdiet arcu luctus. Vestibulum arcu odio, auctor eu urna nec, congue consequat sapien. Sed congue gravida ipsum sit amet vestibulum. Proin eget varius lacus, ac dapibus nisi. Nam sapien massa, semper non lorem a, fringilla dapibus purus. Nulla consequat, ante vitae luctus tincidunt, nisi est vehicula odio, ac aliquet est eros sit amet ex. Ut sit amet dolor in urna pharetra ultrices. Praesent id tristique mi.</p>

            <p>Donec vehicula augue in consectetur fringilla. Phasellus a pellentesque lacus, quis lacinia nisl. Nunc pulvinar purus odio, id rhoncus tellus condimentum at. Nunc eleifend vulputate erat, vel cursus orci tempor vitae. Mauris vehicula, metus sed varius condimentum, arcu massa lacinia ligula, non facilisis dui est mollis leo. Ut tincidunt mi ac dui porttitor malesuada. Sed a interdum mauris, eget tempus lorem. Aliquam erat volutpat. Mauris velit neque, rutrum ut malesuada in, suscipit in quam. Nullam consectetur eros ullamcorper nibh posuere dictum. Curabitur ut risus metus. Sed ultrices nulla tellus, vitae malesuada erat dapibus vitae. Donec pharetra quam at feugiat congue. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
               Quisque ornare ligula quam, vitae finibus est pellentesque id. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent tellus nibh, cursus quis ullamcorper in, accumsan sit amet ligula. Aliquam quis ultrices elit, vitae sagittis tortor. Ut tincidunt orci at aliquam vulputate. Integer sagittis dui eget fermentum pharetra. Vestibulum ut arcu sit amet lacus facilisis elementum nec quis risus. Etiam vitae aliquet libero.</p>
        </div>
    </div>

@endsection