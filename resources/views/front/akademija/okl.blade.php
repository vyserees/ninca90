@extends('layouts.front')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">O klađenju</h1>
            <div class="col-md-8">
                <div class="o-klađenju">
                    <h2>Why do we use it?</h2>
                    <hr>
                    <h3>Et harum quidem rerum facilis est et expedita distinctio.</h3>
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.<hr>
                    <h3>Generated</h3>
                    <p>Quisque ornare ligula quam, vitae finibus est pellentesque id.</p>
                    <p>Phasellus consequat sagittis nisl, nec consequat arcu luctus vitae. Nullam vehicula viverra quam accumsan consectetur.</p>
                    <p><img src="/images/ym.png"> Quisque vel maximus dolor</p>
                    <p>Nulla purus justo, molestie at sem non, efficitur fermentum odio. Nulla vulputate faucibus sollicitudin. Etiam ullamcorper nulla ut nulla tristique aliquam eu eget lectus. Sed sed nisi quis turpis hendrerit viverra.</p>
                    <p><img src="/images/ym.png"> Proin in libero erat</p>
                    <p>Donec volutpat fermentum nulla, eu lobortis diam placerat vitae.</p>
                    <hr>
                </div>
            </div>
            <div class="col-md-4">
                <div class="lepabrena">
                    <img src="/images/cube.png" class="img-responsive">
                </div>
            </div>
            <hr>
            <div class="col-md-8">
                <h3>Proin in libero erat</h3>
                <p>Nulla purus justo, molestie at sem non, efficitur fermentum odio. Nulla vulputate faucibus sollicitudin.</p>
                <p>Quisque ornare ligula quam, vitae finibus est pellentesque id.</p>
                <p><img src="/images/ym.png"> Praesent ac odio sed diam ullamcorper congue.</p>
                <p>Duis dapibus vehicula enim vel commodo.</p>
                <div class="sneki">
                    <img src="/images/mp.jpg" class="img-responsive">
                </div>
                <p>Etiam ullamcorper nulla ut nulla tristique aliquam eu eget lectus. Sed sed nisi quis turpis hendrerit viverra. Nam placerat nibh eget faucibus porta.</p>
                <p>Nulla diam erat, mattis mollis imperdiet et, viverra sit amet nibh:</p>
                <ul>
                    <li>Fusce ac fringilla augue, vitae fermentum quam</li>
                    <li>Vivamus blandit velit rhoncus magna hendrerit porttitor</li>
                    <li>Aenean sagittis et ante vel volutpat</li>
                </ul>
            </div>
            <hr>
            <div class="col-md-4">
                <div class="lepabrena">
                    <img src="/images/sb.png" class="img-responsive">
                </div>
            </div>
            <hr>
            <div class="col-md-8">
                <h3>Vivamus blandit velit rhoncus magna hendrerit porttitor</h3>
                <p>Phasellus non iaculis nibh. Vestibulum id purus lorem. Proin tempus massa at auctor volutpat:</p>
                <p>Curabitur ligula ex, rutrum ut nisi vitae, mattis sagittis quam.</p>
                <p>Phasellus consequat sagittis nisl, nec consequat arcu luctus vitae. Nullam vehicula viverra quam accumsan consectetur. Vivamus sollicitudin id orci finibus suscipit. Maecenas rhoncus fringilla egestas. Donec semper egestas magna in scelerisque. Nullam at laoreet nisi. Maecenas et ultrices neque. Fusce ac fringilla augue, vitae fermentum quam.</p>
                <p>Quisque vel maximus dolor. Donec posuere orci felis, ut consectetur libero commodo et. Aliquam erat volutpat. Nulla molestie eu nisl non elementum. Nulla vitae mauris at ante porttitor egestas ac ac nisl.</p>
                <div class="sneki">
                    <img src="/images/p.jpg" class="img-responsive">
                </div>
                <div class="lepabrena">
                    <img src="/images/aaa.jpg" class="img-responsive">
                </div>
            </div>

        <hr>
        <div class="col-md-4">
            <div class="lepabrena">
                <img src="/images/toys.jpg" class="img-responsive">
            </div>
        </div>
        <hr>
    </div>
    </div>
@endsection