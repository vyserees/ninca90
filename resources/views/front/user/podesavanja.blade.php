@extends('layouts.front')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Moja podešavanja</h1>
        </div>
        <div class="col-md-8">
            <div class="pset-layout user-dets">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 ava-slika">
                        <div class="user-profile">
                            <img src="/images/avatars/{{$user->avatar}}" class="img-responsive">
                        </div>
                        <p>Izmenite profilnu sliku</p>
                        <form action="/profil/editpic" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <input type="file" name="slika" class="form-control upload-ava" required>
                            <input type="submit" class="btn btn-primary" value="Snimite izmenu">
                        </form>
                        <hr>
                        <p>Korisničko ime</p>
                        <div class="input-group">
                            <input type="text" name="username" class="form-control" value="{{$user->username}}">
                            <div class="input-group-addon edit-user">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Lični podaci</h3>
                        <p>Ime</p>
                        <div class="input-group">
                            <input type="text" name="firstname" class="form-control" value="{{$user->firstname}}">
                            <div class="input-group-addon edit-user">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                        <p>Prezime</p>
                        <div class="input-group">
                            <input type="text" name="lastname" class="form-control" value="{{$user->lastname}}">
                            <div class="input-group-addon edit-user">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                        <p>Adresa</p>
                        <div class="input-group">
                            <input type="text" name="address" class="form-control" value="{{$user->details->address}}">
                            <div class="input-group-addon edit-user">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                        <p>Grad</p>
                        <div class="input-group">
                            <input type="text" name="city" class="form-control" value="{{$user->details->city}}">
                            <div class="input-group-addon edit-user">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                        <p>Država</p>
                        <div class="input-group">
                            <input type="text" name="country" class="form-control" value="{{$user->details->country}}">
                            <div class="input-group-addon edit-user">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Interesovanja</h3>
                        <p>Omiljeni sport</p>
                        <div class="input-group">
                            <input type="text" name="sport" class="form-control" value="{{$user->details->sport}}">
                            <div class="input-group-addon edit-user">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                        <p>Omiljeni klub</p>
                        <div class="input-group">
                            <input type="text" name="club" class="form-control" value="{{$user->details->club}}">
                            <div class="input-group-addon edit-user">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            @include('front.sidebar')
        </div>
    </div>


@endsection