@extends('layouts.front')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Moj profil</h1>
        </div>
        <div class="col-md-9">
            <div class="user">
                <img src="/images/avatars/{{$prof->avatar}}" class="img-responsive" alt="avatar">
                <h3>{{$prof->username}}</h3>
            </div>
            <div class="user-dets">
                <h3>Lični podaci</h3>
                <div class="user-dets-list">
                    <p>Adresa<span class="pull-right">{{$prof->details->address ? $prof->details->address : 'nema podataka'}}</span></p>
                    <p>Grad<span class="pull-right">{{$prof->details->city ? $prof->details->city : 'nema podataka'}}</span></p>
                    <p>PB<span class="pull-right">{{$prof->details->pb ? $prof->details->pb : 'nema podataka'}}</span></p>
                    <p>Zemlja<span class="pull-right">{{$prof->details->country ? $prof->details->country : 'nema podataka'}}</span></p>
                </div>
                <h3>Društvene mreže</h3>
                <div class="user-dets-list">
                    <p></p>
                </div>
                <h3>Omiljeno</h3>
                <div class="user-dets-list">
                    <p></p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('front.sidebar')
        </div>
    </div>
@stop