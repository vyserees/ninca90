@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Uslovi korišćenja</h1>
        </div>
        <div class="col-md-8">
                <h3 class="uslov-kori">Uslovi korišćenja ninca90.com web portala</h3>
                Korišćenjem ili pristupanjem web portalu ninca90.com prihvatate uslove korišćenja istog.<hr>

1. Svi komentari i poruke objavljeni na web portalu su privatno mišljenje autora i komentatora i ne predstavljaju stavove vlasnika web portala i njegove administracije.<hr>

2. Vlasnik ninca90.com i redakcija ne odgovaraju za tačnost, potpunost i istinitost sadržine postavljenih poruka i komentara, niti stavova iznetih u njima.<hr>

3. Korišćenjem ovog web portala obavezujete se da nećete postavljati poruke ili komentare koji sadrže uvrede, pretnje, psovke, pornografiju, vulgarne izraze, klevete, pozive na nasilje, izražavanje nacionalne netrpeljivosti, nazivanje pripadnika drugih nacionalnosti pogrdnim imenima. Svi oblici nacionalne, rasne, verske, seksualne, rodne i bilo kakve druge mržnje i/ili netolerancije su zabranjeni.
Takođe je zabranjeno ostavljati komentare koji mogu izazvati svađe, bilo ti direktno ili indirektno. Zabranjeno je postavljati komentare koji nemaju veze sa objavljenom analizom. Sve nepravilnosti možete prijaviti putem opcije za prijavu analize koja se nalazi ispod iste.<hr>

4. Informacija iz privatnog života, odnosno lični pisani zapis (pismo, dnevnik, zabeleška, digitalni zapis i slično), zapis lika (fotografski, crtani, filmski, video, digitalni i slično) i zapis glasa (magnetofonski, gramofonski, digitalni i slično) â€“ ne može se objaviti bez pristanka lica čijeg se privatnog života informacija tiče, odnosno lica čije reči, lik odnosno glas sadrži, ako se pri objavljivanju može zaključiti koje je to lice.<hr>

5. Lični pisani zapisi ne mogu se objaviti ni bez pristanka onoga kome su namenjeni, odnosno onoga na koga se odnose, ako bi objavljivanjem bilo povređeno pravo na privatnost ili koje drugo pravo tog lica.<hr>

6. Po prijavi ili saznanju da je na web portal ninca90.com postavljen, protivno tačkama 4. i 5. materijal koji predstavlja informaciju iz privatnog života, odnosno lični pisani zapis, zapis lika i zapis glasa, administratori ninca90.com će pristupiti njegovom uklanjanju bez odlaganja.<hr>

7. Nije dozvoljena upotreba materijala koji su zaštićeni autorskim pravom, osim ukoliko imate odgovarajuće ovlašćenje za korišćenje tuđeg autorskog dela. Vlasnik i redakcija ninca90.com ne odgovaraju za takve materijale, a odgovornost će snositi isključivo korisnik koji ih je postavio. Po prijavi ili saznanju da je na web portal ninca90.com postavljen materijal čije korišćenje predstavlja povredu tuđeg autorskog prava, administratori će pristupiti njegovom uklanjanju bez odlaganja.<hr>

8. Oglašavanje bilo koje vrste nije dozvoljeno uključujući, ali ne ograničavajući se na komercijalno, političko i religiozno oglašavanje i propagiranje. Postavljanje linkova externih web adresa u kontekstu teksta ili diskusije je dozvoljeno, osim ukoliko se radi o reklamiranju.<hr>

9. Namerno i sistematsko ometanje drugih korisnika neće biti tolerisano, a korisnik koji se ponaša suprotno ovim Pravilima upotrebe biće sankcionisan.<hr>

10. Korisnička imena koja su uvredljiva ili su usmerena protiv neke osobe ili grupe će biti izbrisana.<hr>

11. Ukoliko vidite komentar ili poruku za koji smatrate da je u suprotnosti sa ovim Pravilima upotrebe, bez obzira da li je upućen vama ili nekom drugom korisniku, ili smatrate da je neprimeren, uvredljiv, da ima nedozvoljen sadržaj, da je u pitanju spam ili nešto drugo od prethodno navedenog, molimo vas da obavestite administratora.<hr>

12. Vlasnik i redakcija ninca90.com nemaju obavezu da pojedinačno obrazlažu razloge za uklanjanje poruka i zadržavaju pravo da ukinu svaki nalog korisnika koji krše Pravila upotrebe, bez prethodnog obaveštenja.<hr>

13. Banovanje (sankcionisanje) korisnika može da bude u trajanju od 1, 7, 30 dana ili trajno. Otvaranje novog naloga korisnika koji je privremeno ili trajno sankcionisan nije dozvoljeno i neće biti tolerisano.<hr>

14. Korisnici koji zloupotrebe privatne poruke za slanje pornografskog materijala, pretnje, vređanje i ometanje korisnika biće udaljeni kao korisnici web portala. Neophodno je da nas primalac takvih poruka obavesti o tome slanjem odgovarajuće poruke administratoru web portala, kome možete pisati i ukoliko imate pitanja ili vam je potrebna neka pomoć.<hr>

15. Korisnici koji ostavljaju neprikladne komentare na web portalu i pišu pod pseudonimom (Nadimkom), takođe mogu biti diskriminisani banovanjem IP adrese i tako sebi zabraniti pristup svim sekcijama sajta.<hr>

16. U postupku registracije birate svoje korisničko ime. Pomoću korisničkog imena se registrujete. U cilju vaše zaštite i zaštite vaših podataka obavezujete se da svoju šifru nećete otkriti drugim licima. Takođe se obavezujete da ne koristite naloge drugih lica. Preporučujemo da šifra za vaš nalog bude složena i originalna.<hr>

17. Prilikom registracije za web portal ninca90.com pristajete da administraciji sajta ostavite lične podatke (email i eventualno neki drugi podaci koji nisu obavezni) koje prikupljamo samo radi Vaše registracije i u drugu svrhu se ne mogu koristiti.<hr>

18. U potpunosti ste odgovorni za tačnost podataka koje ste prosledili o sebi. Svaka informacija za koju vlasnik web portala ili redakcija ninca90.com ocene da je nepristojna i u suprotnosti sa postavljenim Pravilima upotrebe biće uklonjena sa ili bez prethodnog obaveštenja korisnika.<hr>

19. ninca90.com štiti privatnost korisnika u najvećoj mogućoj meri. ninca90.com se obavezuje da privatne podatke korisnika neće distribuirati drugim licima, osim uz dozvolu korisnika, ili u slučaju teškog kršenja Pravila upotrebe ili nezakonitih aktivnosti korisnika.<hr>

20. Napominjemo da se svaki komentar ili poruka, vaša IP adresa čuvaju za slučaj da vaša poruka, komentar ili nalog budu ukinuti zbog grubog nepoštovanja i kršenja Pravila upotrebe.<hr>

21. Upozoravamo da je zabranjeno neovlašćeno skidanje i korišćenje u bilo koje svrhe tekstova i fotografija koji budu postavljeni na web portalu. Svaki korisnik koji na ovaj način povredi tuđe autorsko pravo odgovoran je za svu štetu koju ninca90.com pretrpi i dužan je da je nadoknadi.<hr>

22. Vlasnik i redakcija ninca90.com zadržavaju pravo promene Uslova korišćenja web portala bez upozorenja korisnicima. Administraciju ninca90.com možete da kontaktirate putem forme Kontakt.  Korišćenjem bilo kog sadržaja na web portalu smatra se da ste upoznati sa najnovijim pravilima.<hr></p>
        </div>
    <div class="col-md-4">
        @include('front.sidebar')
    </div>


@stop