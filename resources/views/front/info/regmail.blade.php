@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Uspešna registracija</h1>
        </div>
        <div class="col-md-12">
            <p>Hvala Vam što ste se registrovali.</p>
            <p>Poslali smo Vam email sa linkom za aktivaciju vaše email adrese.</p>
            <p>Proverite vaš inbox.</p>
        </div>
    </div>
@stop