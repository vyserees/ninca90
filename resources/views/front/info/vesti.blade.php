@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Vesti</h1>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-6 vesti-info">
                    <h3 class="vesti-info">Domaće sportske novosti</h3><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="https://www.sportske.net/"> sportske.net</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="https://naslovi.net/sport/"> naslovi.net</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="http://www.zurnal.rs/"> žurnal.rs</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="http://sport.blic.rs/"> blic.rs</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="http://www.danas.rs/sport.18.html"> danas.rs</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="http://www.novosti.rs/vesti/sport.2.html"> novosti.rs</a></p><hr>
                </div>
                <div class="col-md-6 vesti-info">
                    <h3 class="vesti-info">Strane sportske novosti</h3><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="http://www.skysports.com/"> skysports.com</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="http://www.bbc.com/sport"> BBC.com</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="http://www.espn.com/"> ESPN.com</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="http://www.independent.co.uk/sport?CMP=ILC-refresh"> independent.co.uk</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="https://www.cbssports.com/"> CBSsports.com</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="http://www.breakingnews.ie/sport/"> breakingnews.ie</a></p><hr>
                        <p><i class="fa fa-newspaper-o"></i> <a target="_blank" href="https://www.theguardian.com/uk/sport"> theguardian.com</a></p><hr>
                    </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('front.sidebar')
        </div>
    </div>
@stop