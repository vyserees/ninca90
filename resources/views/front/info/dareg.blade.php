@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Potvrda profila</h1>
        </div>
    </div>
    <div class="col-md-12">
        <div class="reg-poruka">
            <p>Uspešno ste se registrovali</p>
            <p>Sada se možete ulogovati na svoj nalog.</p>
            <br>
            <a href="/login" class="btn btn-primary">Ulogujte se</a>
        </div>

    </div>

@stop