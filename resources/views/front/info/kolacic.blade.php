@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Uslovi korišćenja kolačića</h1>
        </div>
            <div class="col-md-8">
                <h3 class="kolacic">Korišćenje kolačića na web sajtu ninca90.com</h3>
                <p>ninca90.com koristi kolačiće da bi kolektovali statističke podatke pomoću kojih stalno poboljšavamo korisnički ugođaj. Kolačić je datoteka (fajl) koja se privremeno smešta na Vašem disku za trajno skladištenje podataka. Gotovo svi web sajtovi koriste kolačiće, a neretko se dešava da sajtovi ne funkcionišu u potpunosti bez omogućenih kolačića. Kolačić sadrži pseudoslučajno generisani ID, koji omogućava da se prepozna Vaš računar i da se kolektuju informacije o web stranicama i funkcijama korišćenim od strane Vašeg pretraživača. Međutim, kolačići ne mogu identifikovati Vas, Vaše ime ili adresu. Takođe, kolačići ne mogu biti nosioci virusa i drugih malicioznih programa. Između ostalog, kolačići se mogu koristiti za memorisanje veličine fonta web sajta, ako ste odlučili da ga promenite. Tako da, kada sledeći put posetite web sajt, font će biti veličine koju ste prethodno izabrali.</p><hr>
                <h3 class="kolacic">Kolačići za statističke podatke</h3>
                <p>Dodatno, AlfaŠtampa.rs koristi Google Analytics uslugu za kolektovanje statističkih podataka o ponašanju korisnika na web sajtu. Mi koristimo ove informacije kako bismo razumeli šta korisnici traže i našli najbolji način da im prezentujemo relevantne informacije. Google snima informacije o saobraćaju web lokacije na svojim serverima, ali mi maskiramo poslednjih osam cifara IP adrese kako bismo ih učinili anonimnim. Posledica je da pojedinačni korisnik ne može biti identifikovan na osnovu podataka prikupljenih od strane Google-a. Odbacite ili izbrišite kolačiće Vi uvek možete da odbacite kolačiće na Vašem računaru tako što ćete promeniti podešavanja web pretraživača. Tačna lokacija podešavanja zavisi od pretraživača koji koristite. Međutim, trebalo bi da budete svesni da ako odbacite kolačiće nećete moći da koristite određene funkcije i servise, kao što je na primer naručivanje ili da pristup korisničkom nalogu, i da vidite one koje se u svom funkcionisanju oslanjaju na informacije o izborima koje ste Vi napravili. Svi web pretraživači Vam omogućavaju da izbrišete kolačiće pojedinačno ili sve odjednom. Način na koji se ovo izvodi zavisi od pretraživača koji koristite. Pratite linkove ispod za informacije za pretraživač koji koristite. Imajte na umu da ukoliko koristite više pretraživača, morate izbrisati kolačiće iz svih pretraživača.</p><hr>
            </div>
            <div class="col-md-4">
                @include('front.sidebar')
            </div>
        </div>
@stop