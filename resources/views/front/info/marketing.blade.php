@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Marketing</h1>
            <div class="col-md-8">
                <h3 class="marketing">Marketing</h3>
                <p>
                    Tipodrom.com portal nudi reklamiranje po veoma povoljnim cenama. Tipodrom postoji od 2017 godine, a za to vreme je stekao status jednog od najposećenijih i najaktivnijih kladioničarskih portala u zemljama bivše Jugoslavije (Srbija, Hrvatska, BiH, Makedonija, Crna Gora, Slovenija).</p><hr>
                <h3 class="marketing">Zašto se reklamirati na ninca90.com portalu?</h3>
                    <p>
                        Od osnivanja pa do danas, ninca90.com beleži konstantan rast poseta, o čemu svedoče sajtovi namenjeni za praćenje istih. Posetioci Tipodroma su ponajviše stanovnici (redom) Srbije, Hrvatske, BiH, Makedonije, Crne Gore, Slovenije...

                        ninca90.com nudi reklamiranje kako za organizacije i pravna lica, tako i za pojedince po odličnim uslovima i različitim principima. Postavljajući svoju reklamu na naš portal povećavate zainteresovanost za svoju delatnost prevashodno među osobama starosti od 20 do 30 godina, s obzirom da oni čine najveći broj naših posetilaca.</p><hr>
                <h3 class="marketing">Reklamne pozicije (početna strana i desna kolona)</h3><hr>
                <h3 class="marketing">Reklamne pozicije (analize utakmica - primer)</h3><hr>
            </div>
        <div class="col-md-4">
            @include('front.sidebar')
        </div>
        </div>
    </div>

@stop