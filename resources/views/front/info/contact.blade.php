@extends('layouts.front')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">Kontakt</h1>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-4">
                    <h3 class="kont-info">Kontakt Info</h3>
                    <div class="osnovni-pod">
                        <p><i class="fa fa-user"></i> Budna Sova - Ninca90</p><hr>
                        <p><i class="fa fa-map-marker"></i> Nepoznata ulica bb</p><hr>
                        <p><i class="fa fa-mobile"></i> +381 61 xxx 12 34</p><hr>
                        <p><i class="fa fa-at"></i> <a href="mailto:dojcinovicnemanja@gmail.com"> ninca90@gmail.com</a></p><hr>
                        <p><i class="fa fa-user-circle-o"></i> <a href="https://www.bettingexpert.com/rs/korisnik/profil/ninca90"> bettingexpert.com</a></p><hr>
                    </div>
                </div>
                <div class="col-md-8">
                    <h3 class="pitanja-korisnika">Pitanje</h3>
                    <form action="/contact/send" method="post">
                        <label>Vaše ime</label>
                        <input type="text" name="title" class="form-control">
                        <label>E-mail adresa</label>
                        <input type="text" name="title" class="form-control">
                        <label>Pitanje</label>
                        <textarea name="excerpt" rows="6" class="form-control"></textarea>
                        <hr>
                        <button type="submit" class="btn btn-pitanje">Pitaj</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            @include('front.sidebar')
        </div>
    </div>

@stop