@extends('layouts.front')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">{{$tip->pars->home.' - '.$tip->pars->away}}</h1>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-2">
                    <div class="tip-boxes tip-times">
                        <i class="fa fa-calendar"></i>
                        <p>{{date('d.m.',strtotime($tip->datum))}}</p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="tip-boxes tip-times">
                        <i class="fa fa-clock-o"></i>
                        <p>{{date('H:i',strtotime($tip->datum))}}</p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="tip-boxes tip-dets">
                        <i class="fa fa-money"></i>
                        <p>{{$tip->bets->value}}/10</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="tip-boxes tip-dets">
                        <i class="fa fa-gamepad"></i>
                        <p>{{$tip->games->name}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"><hr></div>
                <div class="col-md-8">
                    <article class="tip-text">
                        {!! $tip->texts()->where('lang','sr')->first()->description !!}
                    </article>
                    <img src="/images/screenshots/{{$tip->texts->tiket}}" alt="Screenshot uplacenog tiketa" class="img-responsive"/>
                </div>
                <div class="col-md-4">
                    <div class="tip-kvota">
                        <img src="/images/kladionice/{{$tip->odds->locations->icon}}" alt="" class="img-responsive">
                        <div class="tip-kvota-broj"><span>{{$tip->odds->value}}</span></div>
                    </div>
                    <p class="tip-profil">
                        <img src="/images/avatars/{{$tip->users->avatar}}" alt="" class="img-responsive"> {{$tip->users->username}}
                    </p>
                    <p class="tip-profil-suc">Broj tipova <span class="pull-right">{{$tip->users->stats->broj}}</span> </p>
                    <p class="tip-profil-suc">Učinak (W-L-V) <span class="pull-right">{{$ucinak}}</span> </p>
                    <p class="tip-profil-suc">Ukupan ulog <span class="pull-right">{{$tip->users->stats->ulog}}</span> </p>
                    <p class="tip-profil-suc">Ukupan profit <span class="pull-right">{{$tip->users->stats->profit}}</span> </p>
                    <p class="tip-profil-suc">Uspešnost <span class="pull-right">{{$tip->users->stats->procenat}}%</span> </p>
                </div>
                <div class="col-md-12"><hr></div>
                <div class="col-md-12">
                    <div class="tip-ostalo">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Komentari</a></li>
                            <li role="presentation" ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Ostale analize tipstera</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Pregled uspešnosti</a></li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="settings">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="koment">
                                            @if(strlen(session('new-blog-com-alert'))>0)
                                                <h3 class="alert alert-warning"><i class="fa fa-warning"></i> {{session('new-blog-com-alert')}}</h3>
                                                <?php session(['new-blog-com-alert'=>'']);?>
                                            @endif
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form action="/analize/newcom" method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="url" value="{{Request::path()}}">
                                                        <input type="hidden" name="aid" value="{{$tip->id}}">
                                                        <textarea name="text" class="form-control" rows="5" placeholder="unesite tekst komentara..." required></textarea>
                                                        <button type="submit" class="pull-right btn btn-primary btn-lg">Postavi komentar</button>
                                                    </form>
                                                </div>
                                                <div class="col-md-12"><hr></div>
                                                <div class="col-md-12">
                                                    @if(count($coms)>0)
                                                        @foreach($coms as $com)
                                                            <div class="koment-users">
                                                                <div class="koment-title">
                                                                    <img src="/images/avatars/{{$com->users->avatar}}">
                                                                    {{$com->users->username}}
                                                                    <span class="pull-right">{{date('H:i - d.m.Y.',strtotime($com->created_at))}}</span>
                                                                </div>
                                                                <article class="koment-text">
                                                                    {!! $com->text !!}
                                                                </article>
                                                            </div>
                                                        @endforeach
                                                        {{$coms->links()}}
                                                    @else
                                                        <p class="blog-single-nocom">Budite prvi koji ćete postaviti komentar</p>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane " id="home">
                                <table class="table table-bordered table-striped">
                                    @foreach($otips as $fin)
                                        <tr>
                                            <td width="5%" style="text-align: center;"><img src="/images/sports/{{$fin->sports()->first()->icon}}" alt="{{$fin->sports()->first()->name}}" title="{{$fin->sports()->first()->name}}" class="img-responsive"></td>
                                            <td width="15%" style="text-align: center;">{{date('d.m. H:i',strtotime($fin->datum))}}</td>
                                            <td>{{$fin->pars->home.' - '.$fin->pars->away}}</td>
                                            <td style="text-align: center;">{{$fin->games->name}}</td>
                                            <td>{{$fin->users->username}}</td>
                                            <td style="text-align: center;">
                                                <a href="/analize/{{$tip->slug}}" class="btn btn-primary btn-xs"><i class="fa fa-search"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <table class="table table-bordered table-striped">
                                    @foreach($oftips as $fin)
                                        <tr>
                                            <td width="5%" style="text-align: center;"><img src="/images/sports/{{$fin->sports()->first()->icon}}" alt="{{$fin->sports()->first()->name}}" title="{{$fin->sports()->first()->name}}" class="img-responsive"></td>
                                            <td width="15%" style="text-align: center;">{{date('d.m. H:i',strtotime($fin->datum))}}</td>
                                            <td>{{$fin->pars->home.' - '.$fin->pars->away}}</td>
                                            <td style="text-align: center;">Nema rezultata</td>
                                            <td style="text-align: center;">{{$fin->games->name}}</td>
                                            <td>{{$fin->users->username}}</td>
                                            <td style="text-align: center;">{!! \App\Icon::where('status',$fin->status)->first()->name !!}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('front.sidebar')
        </div>
    </div>

@endsection