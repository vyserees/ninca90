@extends('layouts.front')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">ANALIZE</h1>
        </div>
        <div class="col-md-9">
            <div class="analize-filteri">
                <div class="row">
                    <div class="col-md-3">
                    <select class="form-control" id="analize-filteri-sport">
                        <option value="0">Svi sportovi</option>
                        @foreach(\App\Sport::all() as $sp)
                            <option @if($sport===$sp['id']) selected="selected"  @endif value="{{$sp->id}}">{{$sp->name}}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="col-md-4">
                    <select class="form-control" id="analize-filteri-vreme">
                        <option value="1" @if($sort==1) selected="selected"  @endif>Vreme početka najranije</option>
                        <option value="2" @if($sort==2) selected="selected"  @endif>Vreme početka najkasnije</option>
                    </select>
                    </div>
                    <div class="col-md-5">
                        {{$tips->links()}}
                    </div>
                </div>
            </div>
            @if(count($tips)>0)
                @foreach($tips as $tip)
                    <div class="tips-layout">
                        <div class="tips-datum">
                            <i class="fa fa-calendar"></i> {{date('d.m.Y.',strtotime($tip->datum))}} <i class="fa fa-clock-o"></i> {{date('H:i',strtotime($tip->datum))}}
                        </div>
                        <div class="tips-klad">
                            <a href="{{\App\Location::find($tip->odds->location)->url}}"><img src="/images/kladionice/{{\App\Location::find($tip->odds->location)->icon}}" alt="Slika kladionice" style="height:40px;"/></a>
                        </div>
                        <div class="tips-profil">
                            <span class="tips-profil-img" title="{{$tip->users->username}}"><a href=""><img src="/images/avatars/{{$tip->users->avatar}}" alt="Slika korisnika" class="img-responsive"></a></span>
                            <span class="tips-yield">{{$tip->users->stats->procenat}}%</span>
                        </div>
                        <div class="tips-par">
                            <a href="/analize/{{$tip->slug}}">
                                <img src="/images/sports/{{$tip->sports()->first()->icon}}" alt="{{$tip->sports()->first()->name}}" title="{{$tip->sports()->first()->name}}">
                                {{$tip->pars->home.' - '.$tip->pars->away}}
                            </a>
                        </div>
                        <div class="tips-dets">
                            <div class="tips-dets-igra">{{$tip->games->name}}</div>
                            <div class="tips-dets-kvota">{{$tip->odds->value}}</div>
                        </div>
                    </div>
                @endforeach
            @else
                <h2>NEMA ANALIZA</h2>
            @endif

            <div class="tips-over">
                <h3>ZAVRŠENI TIPOVI</h3>
                @if(count($fins)>0)
                    <table class="table table-bordered table-striped">
                        @foreach($fins as $fin)
                            <tr>
                                <td width="5%" style="text-align: center;"><img src="/images/sports/{{$fin->sports()->first()->icon}}" alt="{{$fin->sports()->first()->name}}" title="{{$fin->sports()->first()->name}}" class="img-responsive"></td>
                                <td width="15%" style="text-align: center;">{{date('d.m. H:i',strtotime($fin->datum))}}</td>
                                <td>
                                    <a href="/analize/{{$fin->slug}}">{{$fin->pars->home.' - '.$fin->pars->away}}</a>
                                </td>
                                <td style="text-align: center;">
                                    @if(null!==$fin->pars->result)
                                        @if(null!==$fin->pars->period1)
                                            ({{$fin->pars->period1}})&nbsp;
                                        @endif
                                        @if(null!==$fin->pars->period2)
                                            ({{$fin->pars->period2}})&nbsp;
                                        @endif
                                        @if(null!==$fin->pars->period3)
                                            ({{$fin->pars->period3}})&nbsp;
                                        @endif
                                        @if(null!==$fin->pars->period4)
                                            ({{$fin->pars->period4}})&nbsp;
                                        @endif
                                        @if(null!==$fin->pars->period5)
                                            ({{$fin->pars->period5}})&nbsp;
                                        @endif
                                        {{$fin->pars->result}}
                                    @else
                                        {{'nije upisano'}}
                                    @endif
                                </td>
                                <td style="text-align: center;">{{$fin->games->name}}</td>
                                <td>{{$fin->users->username}}</td>
                                <td style="text-align: center;">{!! \App\Icon::where('status',$fin->status)->first()->name !!}</td>
                            </tr>
                        @endforeach
                    </table>
                @else
                    <h2>NEMA ANALIZA</h2>
                @endif

            </div>
        </div>
        <div class="col-md-3">
            @include('front.sidebar')
        </div>
    </div>
@endsection