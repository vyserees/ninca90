@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">Komentari analiza</h3>
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th>Datum</th>
                    <th>Korisnik</th>
                    <th width="40%">Tekst</th>
                    <th>Analiza</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($coms as $com)
                    <tr>
                        <td>{{date('d.m.Y. H:i', strtotime($com->created_at))}}</td>
                        <td>{{$com->users->username}}</td>
                        <td>{!! $com->text !!}</td>
                        <td>{{$com->tips->pars->home.' - '.$com->tips->pars->away}}</td>
                        <td style="text-align: center">
                            @if($com->status==0)
                                <a href="/admin/analize/com/acc/{{$com->id}}" class="btn btn-warning btn-xs" title="Potvrdi komentar"><i class="fa fa-check"></i></a>
                            @else
                                <a class="btn btn-success btn-xs" title="Potvrđen komentar"><i class="fa fa-thumbs-up"></i></a>
                            @endif
                            @if(date('Y-m-d H:i:s')<$com->tips->datum)
                                <a href="/blog/{{$com->tips->slug}}" class="btn btn-default btn-xs" title="Odgovori na komentar"><i class="fa fa-reply"></i></a>
                            @endif
                            <a href="/admin/analize/com/del/{{$com->id}}" class="btn btn-danger btn-xs" title="Obriši komentar"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                @endforeach
                {{$coms->links()}}
                </tbody>
            </table>
        </div>
    </div>
@stop