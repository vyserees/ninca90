@extends('layouts.admin')
@section('content')
    <?php if(isset($_GET['lang'])){$lang = $_GET['lang'];}else{$lang = 'sr';}?>
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">DODAVANJE NOVE ANALIZE</h1>
        </div>
        <div class="col-md-12">
            <div>

                <!-- Nav tabs -->
                <ul class="adm-nav-tabs nav nav-tabs" role="tablist">
                    <li role="presentation" @if($lang==='sr') class="active" @endif><a href="/admin/analize/nova">Srpski</a></li>
                    <li role="presentation" @if($lang==='en') class="active" @endif><a href="/admin/analize/nova?lang=en">Engleski</a></li>
                    <li role="presentation" @if($lang==='fr') class="active" @endif><a href="/admin/analize/nova?lang=fr">Francuski</a></li>
                    <li role="presentation" @if($lang==='es') class="active" @endif><a href="/admin/analize/nova?lang=es">Španski</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="adm-tabs tab-content">
                    <div role="tabpanel" class="tab-pane  @if($lang==='sr') active @endif" id="home">
                        <form action="/admin/analize/add" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="lang" value="{{$lang}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Sport</label>
                                    <select name="sport" class="form-control" required>
                                        <option value="">izaberi sport</option>
                                        @foreach(\App\Sport::all() as $spo)
                                            <option value="{{$spo->id}}">{{$spo->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Datum</label>
                                            <input type="text" name="datum" class="form-control datumi" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Vreme</label>
                                            <input type="text" name="vreme" class="form-control vremena" required>
                                        </div>
                                    </div>
                                    <label>Domacin</label>
                                    <input type="text" name="home" class="form-control" required>

                                    <label>Gost</label>
                                    <input type="text" name="away" class="form-control" required>
                                    <label>Tip</label>
                                    <input type="text" name="tip" class="form-control" required>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Kvota</label>
                                            <input type="text" name="kvota" class="form-control" required>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Ulog</label>
                                            <input type="number" min="1" max="10"  name="ulog" class="form-control" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Kladionica</label>
                                            <select name="kladionica" class="form-control" required>
                                                <option value="">izaberi kladionicu</option>
                                                @foreach(\App\Location::all() as $lok)
                                                    <option value="{{$lok->id}}">{{$lok->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <label>Ucitaj screenshot tiketa</label>
                                    <input type="file" name="tiket" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Sadržaj analize</label>
                                    <div id="srtext"></div>
                                    <input type="hidden" name="content" value="">
                                </div>
                                <div class="col-md-12"><hr></div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-ninca">SNIMI NOVU ANALIZU</button>
                                    <button type="reset" class="btn btn-danger btn-sm pull-right">Obrisi uneto</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane @if($lang==='en') active @endif" id="profile">...</div>
                    <div role="tabpanel" class="tab-pane @if($lang==='fr') active @endif" id="messages">...</div>
                    <div role="tabpanel" class="tab-pane @if($lang==='es') active @endif" id="settings">...</div>
                </div>

            </div>
        </div>
    </div>
    <script>


    </script>
@stop