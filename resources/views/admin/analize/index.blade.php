@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">ANALIZE</h1>
            <ul class="adm-nav-tabs nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Aktivne analize</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Prosle analize</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="adm-tabs-anal tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    @if(count($tips)>0)
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th width="5%">Sport</th>
                                <th width="10%">Vreme</th>
                                <th>Par</th>
                                <th>Tipster</th>
                                <th width="20%">Tip</th>
                                <th width="10%"></th>
                            </tr>
                            @foreach($tips as $tip)
                                <tr>
                                    <td>
                                        <img src="/images/sports/{{$tip->sports()->first()->icon}}" alt="{{$tip->sports()->first()->name}}" class="img-responsive">
                                    </td>
                                    <td>{{date('d.m.',strtotime($tip->datum)).' '.date('H:i',strtotime($tip->vreme))}}</td>
                                    <td>{{$tip->pars->home.'-'.$tip->pars->away}}</td>
                                    <td>{{$tip->users->username}}</td>
                                    <td>{{$tip->games->name}}</td>
                                    <td style="text-align: center">
                                        <a href="/admin/analize/izmeni/{{$tip->id}}" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></a>
                                        <a onclick="return confirm('Da li ste sigurni?')" href="/admin/analize/obrisi/{{$tip->id}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <h1 class="alert alert-warning">NEMA AKTIVNIH ANALIZA</h1>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    @if(count($olds)>0)
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th width="5%">Sport</th>
                                <th width="10%">Vreme</th>
                                <th>Par</th>
                                <th>Tipster</th>
                                <th width="15%">Tip</th>
                                <th width="15%">Rezultat</th>
                                <th width="5%"></th>
                            </tr>
                            @foreach($olds as $tip)
                                <tr>
                                    <td>
                                        <img src="/images/sports/{{$tip->sports()->first()->icon}}" alt="{{$tip->sports()->first()->name}}" class="img-responsive">
                                    </td>
                                    <td>{{date('d.m.',strtotime($tip->datum)).' '.date('H:i',strtotime($tip->vreme))}}</td>
                                    <td>{{$tip->pars->home.'-'.$tip->pars->away}}</td>
                                    <td>{{$tip->users->username}}</td>
                                    <td>{{$tip->games->name}}</td>
                                    <td>
                                        @if(null!==$tip->pars->result)
                                                @if(null!==$tip->pars->period1)
                                                    ({{$tip->pars->period1}})&nbsp;
                                                @endif
                                                @if(null!==$tip->pars->period2)
                                                    ({{$tip->pars->period2}})&nbsp;
                                                @endif
                                                @if(null!==$tip->pars->period3)
                                                    ({{$tip->pars->period3}})&nbsp;
                                                @endif
                                                @if(null!==$tip->pars->period4)
                                                    ({{$tip->pars->period4}})&nbsp;
                                                @endif
                                                @if(null!==$tip->pars->period5)
                                                    ({{$tip->pars->period5}})&nbsp;
                                                @endif
                                            {{$tip->pars->result}}
                                        @else
                                        {{'nije upisano'}}
                                        @endif
                                    </td>
                                    <td style="text-align: center;">
                                        @if(null!==$tip->pars->result)
                                            {!! \App\Icon::where('status',$tip->status)->first()->name !!}
                                        @else
                                            <a href="javascript:void(0)" class="btn btn-default btn-xs show-tip" data-target="#tip{{$tip->id}}"><i class="fa fa-search"></i></a>
                                        @endif
                                    </td>

                                </tr>
                                <tr id="tip{{$tip->id}}" class="tip-dets" style="display:none;background: #aaa;">
                                    <td colspan="7" style="padding:10px 20px;">
                                        <div class="row" style="background: #fff;">
                                            <form action="/admin/analize/result" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="anid" value="{{$tip->id}}">
                                                <div class="col-md-2">
                                                    <label>Period 1</label>
                                                    <input type="text" name="period1" class="form-control">
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Period 2</label>
                                                    <input type="text" name="period2" class="form-control">
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Period 3</label>
                                                    <input type="text" name="period3" class="form-control">
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Period 4</label>
                                                    <input type="text" name="period4" class="form-control">
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Period 5</label>
                                                    <input type="text" name="period5" class="form-control">
                                                </div>
                                                <div class="col-md-2">
                                                    <label>FT</label>
                                                    <input type="text" name="result" class="form-control" required>
                                                </div>
                                                <div class="col-md-12"><hr></div>
                                                <div class="col-md-12">
                                                    <div class="radio-inline">
                                                        <input type="radio" name="status" value="1" required> Pogodjen
                                                    </div>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="status" value="2" required> Promasen
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="status" value="3" required> Void
                                                    </label>
                                                </div>
                                                <div class="col-md-12" style="margin:10px 0;">
                                                    <input type="submit" class="btn btn-primary btn-lg" value="EVIDENTIRAJ ANALIZU">
                                                </div>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <h1 class="alert alert-warning">NEMA PROSLIH ANALIZA</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop