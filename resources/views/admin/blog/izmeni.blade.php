@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">Izmena bloga</h3>
            <div>

                <!-- Nav tabs -->
                <ul class="adm-nav-tabs nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Srpski</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Engleski</a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Francuski</a></li>
                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Španski</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="adm-tabs tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <form action="/admin/blog/edit" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="blogid" value="{{$blog->id}}">
                            <label>Naslov</label>
                            <input type="text" name="title" class="form-control" value="{{$blog->title}}">
                            <label>Kratak tekst</label>
                            <textarea name="excerpt" rows="6" class="form-control">{{$blog->excerpt}}</textarea>
                            <label>Sadržaj bloga</label>
                            <div id="srtext">{!! $blog->content !!}</div>
                            <input type="hidden" name="content" value="">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="/images/blog/{{$blog->image}}" alt="{{$blog->title}}" class="img-responsive">
                                </div>
                                <div class="col-md-8">
                                    <label>Izmeni sliku</label>
                                    <input type="file" name="image" class="form-control fileinput"><br>
                                </div>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-primary btn-ninca">Sačuvaj izmene</button>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">...</div>
                    <div role="tabpanel" class="tab-pane" id="messages">...</div>
                    <div role="tabpanel" class="tab-pane" id="settings">...</div>
                </div>

            </div>
        </div>

    </div>
@stop