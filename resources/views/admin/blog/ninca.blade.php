@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-6">

        </div>
        <div class="col-md-6">
            <div class="blog-form">
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Srpski</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Engleski</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Francuski</a></li>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Španski</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <form action="/admin/blog/ninca/new" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <label>Naslov</label>
                                        <input type="text" name="title" class="form-control">
                                        <label>Kratak tekst</label>
                                        <textarea name="excerpt" rows="6" class="form-control"></textarea>
                                        <label>Sadržaj bloga</label>
                                        <div id="srtext"></div>
                                        <input type="hidden" name="content" value="">
                                        <label>Dodaj sliku</label>
                                        <input type="file" name="images[]" class="form-control fileinput" multiple><br>
                                        <button type="submit" class="btn btn-ninca">Objavi blog</button>
                                    </form>
                                </div>
                            <div role="tabpanel" class="tab-pane" id="profile">...</div>
                            <div role="tabpanel" class="tab-pane" id="messages">...</div>
                            <div role="tabpanel" class="tab-pane" id="settings">...</div>
                            </div>
                        </div>


        </div>

    </div>
@stop