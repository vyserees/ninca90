@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">Lista blogova</h3>
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <th width="10%">Slika</th>
                <th width="20%">Naslov</th>
                <th width="45%">Kratak tekst</th>
                <th>Datum</th>
                <th></th>
            </tr>
            @foreach($blogs as $blog)
                <tr>
                    <td>
                        <img style="width: 150px" src="/images/blog/{{$blog->image}}">
                    </td>
                    <td>{{$blog->title}}</td>
                    <td>{{$blog->excerpt}}</td>
                    <td style="text-align: center">{{date('d.m.Y.',strtotime($blog->created_at))}}</td>
                    <td style="text-align: center">
                        <a href="/admin/blog/izmeni/{{$blog->id}}" class="btn btn-success btn-xs" title="Izmeni blog"><i class="fa fa-edit"></i> </a>
                        <a href="/admin/blog/obrisi/{{$blog->id}}" onclick="return confirm('Da li ste sigurni?')" class="btn btn-danger btn-xs" title="Obrisi blog"><i class="fa fa-trash-o"></i> </a>
                    </td>
                </tr>
            @endforeach
        </table>
        {{$blogs->links()}}
    </div>

</div>
@stop