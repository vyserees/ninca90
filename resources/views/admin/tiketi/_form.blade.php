<?php $c=1;?>
@for($i=1;$i<=$br;$i++)
    <div class="col-md-12"><p class="adm-tiketi-novi-par">Par {{$c}}</p></div>
        <div class="col-md-12">
            <label>Sport</label>
            <select name="sport<?=$c?>" class="form-control" required>
                <option value="">izaberi sport</option>
                @foreach(\App\Sport::all() as $sp)
                    <option value="{{$sp->id}}">{{$sp->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label>Domacin</label>
            <input type="text" name="par_home{{$i}}" class="form-control" required>
        </div>
        <div class="col-md-6">
            <label>Gost</label>
            <input type="text" name="par_away{{$i}}" class="form-control" required>
        </div>
        <div class="col-md-3">
            <label>Kvota</label>
            <input type="text" name="kvota{{$i}}" class="form-control" required>
        </div>
        <div class="col-md-3">
            <label>Tip</label>
            <input type="text" name="tip{{$i}}" class="form-control" required>
        </div>
        <div class="col-md-3">
            <label>Datum</label>
            <input type="text" name="datum{{$i}}" class="form-control datumi" required>
        </div>
        <div class="col-md-3">
            <label>Vreme</label>
            <input type="text" name="vreme{{$i}}" class="form-control vremena" required>
        </div>
        <div class="col-md-12"><hr></div>
        <?php $c++;?>
@endfor
<script>

    var sati=['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23'];
    var minuti=['00','15','30','45'];
    var dozv=[];
    for(var i=0; i<sati.length; i++){
        for(var j=0; j<minuti.length; j++){
            dozv.push(sati[i]+':'+minuti[j]);
        }
    }
    /*Datumi i vremena*/
    $('.datumi').datetimepicker({
        timepicker:false,
        format:'d.m.Y'
    });
    $('.vremena').datetimepicker({
        datepicker:false,
        format:'H:i',
        allowTimes:dozv
    });
</script>