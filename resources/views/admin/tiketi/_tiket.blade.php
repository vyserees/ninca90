<div class="row">
    <div class="col-md-12">
        <?php switch ($tik->tip){
            case '1':
                $title='Singl';
                break;
            case '2':
                $title='Dubl';
                break;
            default:
                $title='tiket';
        } ?>
        <div class="tiket">
            <h1 class="tiket-3">{{$title}}</h1>
            <article class="tiket-tip">
                <table class="table tiket-table">
                    <tr>
                        <th style="text-align: center;" width="5%"></th>
                        <th style="text-align: center;" width="10%">Datum</th>
                        <th style="text-align: center;" width="10">Vreme</th>
                        <th>Par</th>
                        <th style="text-align: center;" width="10%">Tip</th>
                        <th style="text-align: center;" width="10%">Kvota</th>
                    </tr>
                    @foreach($tik->ticketdetails as $td)
                        <tr>
                            <td style="text-align: center;"><img class="img-responsive" src="/images/sports/{{$td->sporticon->icon}}"/></td>
                            <td style="text-align: center;">{{date('d.m.',strtotime($td->datum))}}</td>
                            <td style="text-align: center;">{{date('H:i',strtotime($td->vreme))}}</td>
                            <td>{{$td->par_home}} - {{$td->par_away}}</td>
                            <td style="text-align: center;">{{$td->tip}}</td>
                            <td style="text-align: center;">{{$td->kvota}}</td>
                        </tr>
                    @endforeach
                </table>
            </article>
            <hr style="border-color: #49274a;">
            <article class="tiket-uplata">
                <p>Kladionica: Soccer-bet<span class="pull-right">Ukupna kvota: {{$tik->kvota}}
	    			</span></p>
                <p style="text-align: right">Ulog: {{$tik->verovatnoca}}</p>
            </article>
        </div>
    </div>
</div>
