@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-title">TIKETI</h1>
        </div>
        <div class="col-md-6">
            <table class="table table-bordered table-striped table-hover adm-tik-table">
                <tr>
                    <th colspan="6" style="text-align: left"><h3>Lista tiketa</h3></th>
                </tr>
                <tr>
                    <th>Broj tiketa</th>
                    <th>Tipova</th>
                    <th>Datum</th>
                    <th>Kvota</th>
                    <th></th>
                </tr>
                @foreach($tiketi as $tiket)
                    <tr>
                        <td>
                            <a href="javascript:void(0)" class="show-tiket" data-tik="{{$tiket->id}}">{{$tiket->number}}</a>
                        </td>
                        <td>{{$tiket->tip}}</td>
                        <td>{{date('d.m.Y.',strtotime($tiket->created_at))}}</td>
                        <td>
                            {{$tiket->kvota}}
                        </td>
                        <td>
                            @if($tiket->pocetna==0)
                                <a href="/admin/tiketi/na-pocetnu/{{$tiket->id}}" title="Stavi ovaj tiket na pocetnu" class="btn btn-info btn-xs"><i class="fa fa-home"></i> </a>
                            @else
                                <a class="btn btn-success btn-xs" title="Ovaj tiket je na pocetnoj"><i class="fa fa-home"></i> </a>
                            @endif
                            <a href="/admin/tiketi/brisi/{{$tiket->id}}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-md-6">
            <div class="tiketi-novi">
                <h3>Dodavanje novog tiketa</h3>
                <div class="row">
                    <form action="/admin/tiketi/add" method="post">
                        {{csrf_field()}}
                        <div class="col-md-12">
                            <label for="vrsta">Vrsta tiketa</label>
                            <input id="vrsta" type="text" name="vrsta" class="form-control" maxlength="10" required>
                        </div>

                        <div class="col-md-6">
                            <label for="tiket-parovi">Broj parova</label>
                            <select name="broj_parova" id="tiket-parovi" class="form-control" required>
                                <option value="">izaberi broj parova</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Datum prikazivanja</label>
                            <input type="text" name="datum_prikaz" class="form-control datumi" required>
                        </div>
                        <div class="col-md-12"><hr></div>
                        <div class="tiketi-novi-tipovi"></div>
                        <div class="col-md-8">
                            <label for="tiket-klad">Kladionica</label>
                            <select id="tiket-klad" name="kladionica" class="form-control" required>
                                <option value="">izaberi kladionicu</option>
                                @foreach(\App\Location::all() as $kl)
                                    <option value="{{$kl->id}}">{{$kl->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="verovatnoca">Ulog</label>
                            <input id="verovatnoca" type="number" max="10" min="1" name="verovatnoca" class="form-control" required>
                        </div>
                        <div class="col-md-12">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <input type="checkbox" name="pocetna"> Stavi ovaj tiket na pocetnu
                            </div>
                        </div>
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-ninca">Potvrdi tiket</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="tiket-view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">PREGLED TIKETA</h4>
                </div>
                <div class="modal-body">
                    <div class="tiket-view"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ZATVORI</button>
                </div>
            </div>
        </div>
    </div>

@stop