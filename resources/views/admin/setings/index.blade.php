@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">Podešavanje profila</h3>
        </div>
        <div class="col-md-8">
            <div class="pod-meni">
                <div class="row">
                    <p style="font-size: 16px; text-align: center;">Admin podešavanja</p>
                    <div class="col-md-6 col-md-offset-3 ava-slika">
                        <div class="user-profile">
                            <img src="/images/avatars/{{\Auth::user()->avatar}}" class="img-responsive">
                        </div>
                        <p>Izmeni profilnu sliku</p>
                        <form action="/admin/podesavanja/editpic" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input name="slika" type="file" class="form-control upload-ava" value="Izaberi avatar">
                            <input type="submit" class="submit-ava" name="izmenava" value="Izmeni avatar">
                        </form>
                    </div>
                    <div class="col-md-6 col-md-offset-3">
                        <p>Korisničko ime</p>
                        <div class="input-group">

                            <input type="text" name="username" class="form-control" value="{{\Auth::user()->username}}">
                            <div class="input-group-addon change-user" data-uid="{{\Auth::user()->id}}">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-3">
                        <p>Lozinka</p>
                        <div class="input-group">
                            <input type="text" name="" class="form-control" value="">
                            <div class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop