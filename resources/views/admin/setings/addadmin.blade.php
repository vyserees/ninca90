@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">Dodeli status</h3>
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th width="25%">Datum</th>
                    <th>Korisnik</th>
                    <th width="25%"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($usrs as $usr)
                    <tr>
                        <td>{{date('d.m.Y. H:i', strtotime($usr->created_at))}}</td>
                        <td>{{$usr->username}}</td>
                        <td style="text-align: center">
                            @if($usr->role==='U')
                            <a href="/admin/addadmin/{{$usr->id}}" class="btn btn-success btn-xs">Dodeli status administratora</a>
                            @else
                            <a href="/admin/deladmin/{{$usr->id}}" class="btn btn-danger btn-xs">Oduzmi status administratora</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                {{$usrs->links()}}
                </tbody>
            </table>
        </div>
    </div>

@stop