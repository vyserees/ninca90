@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">Tabele sportova i kladinica</h3>
        </div>
        <div class="col-md-6">
            <div class="pod-tabela">
                <h4 class="page-title">Sportovi</h4>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Naziv sporta</th>
                        <th width="7%">Slika sporta</th>
                        <th width="20%" style="text-align: center;">Obriši</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sps as $sp)
                    <tr>
                        <td>{{$sp->name}}</td>
                        <td><img src="/images/sports/{{$sp->icon}}" class="img-responsive"></td>
                        <td style="text-align: center">
                            <a href="/admin/podesavanja/dellsport/{{$sp->id}}" class="btn btn-danger btn-xs">Obriši</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <form action="/admin/podesavanja/addsport" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input name="ime" type="text" class="form-control dodaj-ime" placeholder="Unesi ime sporta" required>
                    <input name="slika" type="file" class="form-control upload-img" value="Dodaj sliku" required>
                    <input type="submit" class="btn btn-primary" name="izmenimg" value="Dodaj sport">
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="pod-tabela">
                <h4 class="page-title">Kladionice</h4>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Naziv kladionice</th>
                        <th width="10%">Slika kladionice</th>
                        <th width="20%" style="text-align: center;">Obriši</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($klads as $klad)
                    <tr>
                        <td>{{$klad->name}}</td>
                        <td><img src="/images/kladionice/{{$klad->icon}}" class="img-responsive"></td>
                        <td style="text-align: center">
                            <a href="admin/podesavanja/dellklad/{{$klad->id}}" class="btn btn-danger btn-xs">Obriši</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <form action="/admin/podesavanja/addklad" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input name="ime" type="text" class="form-control" placeholder="Unesi ime kladionice" required>
                    <input name="link" type="text" class="form-control" placeholder="Unesi link kladionice" required>
                    <input name="slika" type="file" class="form-control upload-img" value="Dodaj sliku" required>
                    <input type="submit" class="btn btn-primary" name="izmenimg" value="Dodaj kladionicu">
                </form>
            </div>
        </div>







@stop