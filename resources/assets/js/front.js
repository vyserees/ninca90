/**
 * Created by ruy on 10.10.17..
 */

$(document).ready(function(){

    /*eksterne akcije*/

    var sati=['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23'];
    var minuti=['00','15','30','45'];
    var dozv=[];
    for(var i=0; i<sati.length; i++){
        for(var j=0; j<minuti.length; j++){
            dozv.push(sati[i]+':'+minuti[j]);
        }
    }
    /*Datumi i vremena*/
    $('.datumi').datetimepicker({
        timepicker:false,
        format:'d.m.Y'
    });
    $('.vremena').datetimepicker({
        datepicker:false,
        format:'H:i',
        allowTimes:dozv
    });

    /*eksterne akcije*/

    $('#promo-show').click(function(){
        $('.sb-promo').animate({left:'50px'},'slow');
    });
    $('.promo-hide').click(function () {
        $('.sb-promo').animate({left:'-250px'},'slow');
    });
    if($('#srtext').length){
        $('#srtext').summernote({
            height:400,
            focus:false,
            tabsize:2
        });
    }

    $('.fileinput').fileinput({
        showUpload:false
    });
    $('.btn-ninca').click(function(){
        var htm = $('#srtext').summernote('code');
        $('[name="content"]').val(htm);
    });
    $('.blog-owl').slick({
        autoplay:true,
        fade:true
    });
    $('.home-owl').slick({
        autoplay:true,
        fade:false
    });

    /*izbor broja tipova u tiketu u adminu*/
    $('#tiket-parovi').change(function(){
        var br = $(this).val();
        if(br!=''){
            $.ajax({
                url:'/ajax/gettips',
                type:'post',
                data:{br:br},
                success:function(data){
                    $('.tiketi-novi-tipovi').html(data);
                }
            });
        }
    });

    /*pregled tiketa na adminu*/
    $('.show-tiket').click(function(){
        var tid = $(this).attr('data-tik');
        $.ajax({
            url:'/ajax/showtik',
            type:'post',
            data:{tid:tid},
            success:function(data){
                $('.tiket-view').html(data);
                $('#tiket-view').modal('show');
            }
        });
    });

    /*podmeni menadžment*/

    $('.haspodmeni').mouseenter(function () {
        $(this).children('.podmeni').show();
    });
    $('.haspodmeni').mouseleave(function () {
        $(this).children('.podmeni').hide();
    });

    /*pregled detalja analiza na adminu*/
    $(document).on('click','.show-tip',function(){
        var targ = $(this).attr('data-target');
        $('.tip-dets').not(targ).hide();
        $(targ).animate({height:'toggle'},'fast');
    });

    /*logout dugme*/
    $('.logout-btn').click(function () {
        $('#logout-form').submit();
    });

    /*klik na izmenu podatka korisnika*/
    $('.change-user').click(function () {
       var col = $(this).siblings('input').attr('name');
       var uid = $(this).attr('data-uid');
       var val = $(this).siblings('input').val();
       $.ajax({
           url: '/ajax/changeuserdata',
           type: 'POST',
           data: { col:col, uid:uid, val:val},
           success: function () {
               /*
               swal({
                   title: 'Uspešno ste izmenili podatak',
                   type: 'success'
               });
               */
               alert('Uspešno ste izmenili podatak');
           }
       });
    });

    $('.playowl').click(function () {
        var audio = document.getElementById('audio');
        audio.play();
    });
    /*FILERI ANALIZA*/
    $('#analize-filteri-sport').change(function () {
        var spid = $(this).val();
        var soid = $('#analize-filteri-vreme').val();
        location.assign('/analize/filter/sport/'+spid+'/sort/'+soid);
    });
    $('#analize-filteri-vreme').change(function () {
        var spid = $('#analize-filteri-sport').val();
        var soid = $(this).val();
        location.assign('/analize/filter/sport/'+spid+'/sort/'+soid);
    });

    /*to the top button*/
    $('#to-top').click(function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });

});
/*zvuk sove*/
/*
function play() {
    var audio = document.getElementById('audio');
    if (audio.paused) {
        audio.play();

    }else{
        audio.pause();
        audio.currentTime = 0

    }
}
*/
