<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStat extends Model
{
    protected $fillable = ['user_id','broj','ulog','procenat','profit'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
