<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipText extends Model
{
    protected $fillable = ['tip_id','description','tiket','lang'];

    public function tips(){
        return $this->belongsTo('App\Tip','tip_id');
    }
}
