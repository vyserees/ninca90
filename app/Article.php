<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['user_id','title','slug','excerpt','content','image'];

    public function users(){
        return $this->belongsTo('App\Users','user_id');
    }
    public function comments(){
        return $this->hasMany('App\ArticleComment');
    }
}
