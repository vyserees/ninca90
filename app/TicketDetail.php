<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketDetail extends Model
{
    protected $fillable = ['ticket_id','sport','datum','vreme','par_home','par_away','tip','kvota','ht','ft'];

    public function tickets(){
        return $this->belongsTo('App\Ticket','ticket_id');
    }
    public function sporticon(){
        return $this->belongsTo('App\Sport','sport');
    }
}
