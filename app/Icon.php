<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icon extends Model
{
    protected $fillable = ['status','name'];

    public function tips(){
        return $this->belongsTo('App\Tip','status');
    }
}
