<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipComment extends Model
{
    protected $fillable = ['tip_id', 'user_id', 'text', 'status'];

    public function tips(){
        return $this->belongsTo('App\Tip','tip_id');
    }
    public function users(){
        return $this->belongsTo('App\User','user_id');
    }
}
