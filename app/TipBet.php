<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipBet extends Model
{
    protected $fillable = ['tip_id','value'];

    public function tips(){
        return $this->belongsTo('App\Tip','tip_id');
    }
}
