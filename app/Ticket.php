<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['number','tip','vrsta', 'datum','kvota','verovatnoca','kladionica','uplata','status'];

    public function ticketdetails(){
        return $this->hasMany('App\TicketDetail');
    }
}
