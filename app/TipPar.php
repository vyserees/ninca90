<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipPar extends Model
{
    protected $fillable = ['tip_id','home','away','period1','period2','period3','period4','period5','result'];

    public function tips(){
        return $this->belongsTo('App\Tip','tip_id');
    }
}
