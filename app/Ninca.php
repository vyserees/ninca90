<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ninca extends Model
{
    protected $fillable = ['title','slug','excerpt','content','image'];
}
