<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipGame extends Model
{
    protected $fillable = ['tip_id','name'];

    public function tips(){
        return $this->belongsTo('App\Tip','tip_id');
    }
}
