<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'firstname','lastname','email', 'password','role','avatar','email_code','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function articles(){
        return $this->hasMany('App\Article');
    }
    public function comments(){
        return $this->hasMany('App\ArticleComment');
    }
    public function details(){
        return $this->hasOne('App\UserDetail');
    }
    public function tipcoms(){
        return $this->hasMany('App\TipComment');
    }
    public function stats(){
        return $this->hasOne('App\UserStat');
    }
}
