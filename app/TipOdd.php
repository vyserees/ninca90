<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipOdd extends Model
{
    protected $fillable = ['tip_id','value','location'];

    public function tips(){
        return $this->belongsTo('App\Tip','tip_id');
    }
    public function locations(){
        return $this->belongsTo('App\Location','location');
    }
}
