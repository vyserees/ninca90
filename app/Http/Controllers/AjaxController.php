<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function getTips(Request $request){
        $br = $request['br'];
        return view('admin.tiketi._form',compact('br'));
    }
    public function showTik(Request $request){
        $tik = Ticket::find($request['tid']);
        return view('admin.tiketi._tiket',compact('tik'));
    }
    public function changeUserData(Request $request){
        DB::table('users')
            ->where('id', $request['uid'])
            ->update([$request['col'] => $request['val']]);


    }
}