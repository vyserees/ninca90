<?php

namespace App\Http\Controllers;

use App\Ninca;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Article;
use App\Ticket;
use App\Tip;
use App\TipBet;
use App\TipGame;
use App\TipOdd;
use App\TipPar;
use App\TipText;
use App\Sport;
use App\Location;
use App\ArticleComment;
use App\TipComment;
use Carbon\Carbon;
use App\UserDetail;
use App\UserStat;
use App\Mail\Register;

class FrontController extends Controller
{
    public function home()
    {
        $tips = Tip::where('datum','>',date('Y-m-d H:i:s'))->orderBy('datum')->get();
        $tik= Ticket::where('pocetna', '1')->first();
        $blogs=Article::orderBy('created_at', 'desc')->limit(3)->get();
        $stat = User::find(3)->stats;
        $ucinak = $this->drawScore(3);
        //$blogs=Article::
        return view('front.index', compact('tips', 'tik', 'blogs','stat','ucinak'));
    }

    public function blog()
    {
        $nin = Ninca::first();
        $blogs = Article::orderBy('created_at', 'desc')->paginate(5);
        return view('front.blog.index', compact('blogs', 'nin'));
    }

    public function blogSingle($slug)
    {
        $blogs = Article::orderBy('created_at', 'desc')->limit(3)->get();
        $blog = Article::where('slug', $slug)->first();
        $coms = $blog->comments()->where('status',1)->orderBy('created_at','desc')->paginate(6);
        return view('front.blog.single', compact('blog', 'blogs','coms'));
    }
    public function blogNewcom(Request $request){
        ArticleComment::create([
            'article_id'=>$request['aid'],
            'user_id'=>\Auth::user()->id,
            'text'=>$request['text']
        ]);
        session(['new-blog-com-alert'=>'Vaš komentar je evidentiran i čeka odobrenje administratora!']);
        return redirect($request['url']);
    }
    public function blogNinca90()
    {
        return view('front.blog.ninca90');
    }

    public function akademija()
    {
        return view('front.akademija');
    }
    public function akademijaOkl()
    {
        return view('front.akademija.okl');
    }
    public function akademijaSigurica()
    {
        return view('front.akademija.sigurica');
    }
    public function akademijaRizici()
    {
        return view('front.akademija.rizici');
    }

    public function kladSoccer()
    {
        return view('front.kladionice.soc');
    }

    public function tipovi()
    {
        $datum = date('Y-m-d');
        $tiks = Ticket::where('datum', date('Y-m-d'))->get();
        $days = $this->calcDate();
        return view('front.tipovi.index', compact('tiks','datum','days'));
    }
    public function tipoviDani($datum)
    {
        $tiks = Ticket::where('datum', $datum)->get();
        $days = $this->calcDate();
        return view('front.tipovi.index', compact('tiks','datum','days'));
    }

    public function analize()
    {
        $tips = Tip::where('datum','>',date('Y-m-d H:i:s'))->orderBy('datum')->paginate(4);
        $fins = Tip::where('datum','<=',date('Y-m-d H:i:s'))->orderBy('datum')->get();
        $sport = 0;$sort = 0;
        return view('front.analize.index',compact('tips','fins','sport','sort'));
    }
    public function analizeSingle($anal)
    {
        $tip = Tip::where('slug',$anal)->first();
        $coms = $tip->tipcoms()->where('status',1)->orderBy('created_at','desc')->paginate(6);
        $otips = Tip::where('user_id','<>',$tip->user_id)->where('datum','>',date('Y-m-d H:i:s'))->orderBy('datum')->get();
        $oftips = Tip::where('user_id',$tip->user_id)->where('datum','<',date('Y-m-d H:i:s'))->orderBy('datum')->get();
        $ucinak = $this->drawScore($tip->user_id);
        return view('front.analize.single',compact('tip','coms','otips','oftips','ucinak'));
    }
    public function analizeNewcom(Request $request){
        TipComment::create([
            'tip_id'=>$request['aid'],
            'user_id'=>\Auth::user()->id,
            'text'=>$request['text']
        ]);
        session(['new-blog-com-alert'=>'Vaš komentar je evidentiran i čeka odobrenje administratora!']);
        return redirect($request['url']);
    }
    public function analizeFilter($spval,$soval)
    {
        $sport = $spval;$sort = $soval;
        switch($soval){
            case 1:
                if($spval>0){
                    $tips = Sport::find($spval)->tips()->orderBy('datum')->paginate(4);
                }else{
                    $tips = Tip::orderBy('datum')->paginate(4);
                }
                break;
            default:
                if($spval>0){
                    $tips = Sport::find($spval)->tips()->orderBy('datum','desc')->paginate(4);
                }else{
                    $tips = Tip::orderBy('datum','desc')->paginate(4);
                }

        }
        $fins = Tip::where('datum','<=',date('Y-m-d H:i:s'))->orderBy('datum')->get();
        return view('front.analize.index',compact('tips','fins','sport','sort'));
    }
    public function userMojProfil()
    {
        $prof = \Auth::user();
        return view('front.user.profil',compact('prof'));
    }
    public function userMojaPodesavanja()
    {
        $user = \Auth::user();
        return view('front.user.podesavanja',compact('user'));
    }
    public function userEditpic(Request $request){
        $file = Input::file('slika');
        $filename = rand(1111, 9999).preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['slika']['name']);
        $file->move('images/avatars/',$filename);
        Image::make('images/avatars/'.$filename)->resize(360,360)->save('images/avatars/'.$filename);
        $user = \Auth::user();
        $user->avatar = $filename;
        $user->save();
        return redirect('/moja-podesavanja');
    }

    public function soccer()
    {
        return view('front.info.soccer');
    }
    public function kontakt()
    {
        return view('front.info.contact');
    }
    public function onama()
    {
        return view('front.info.onama');
    }
    public function vesti()
    {
        return view('front.info.vesti');
    }
    public function fantasy()
    {
        return view('front.info.fantasy');
    }
    public function  mvp()
    {
        return view('front.info.mvp');
    }
    public function uslovi()
    {
        return view('front.info.uslovi');
    }
    public function pravila()
    {
        return view('front.info.pravila');
    }
    public function marketing()
    {
        return view('front.info.marketing');
    }
    public function kolacic()
    {
        return view('front.info.kolacic');
    }

    /*RAČUNANJE DANA ZA TIKET*/
    private function calcDate()
    {
        $datum  = Carbon::now();

        switch(date('l')){
            case 'Monday';
            return [
                'Petak'=>$datum->subDays(3)->toDateString(),
                'Subota'=>$datum->addDays(1)->toDateString(),
                'Nedelja'=>$datum->addDays(1)->toDateString(),
                'Ponedeljak'=>date('Y-m-d')
            ];
            case 'Tusday';
                return [
                    'Utorak'=>date('Y-m-d'),
                    'Sreda'=>$datum->addDays(1)->toDateString(),
                    'Četvrtak'=>$datum->addDays(1)->toDateString()
                ];
            case 'Wednesday';
                return [
                    'Utorak'=>$datum->subDays(1)->toDateString(),
                    'Sreda'=>date('Y-m-d'),
                    'Četvrtak'=>$datum->addDays(2)->toDateString()
                ];
            case 'Thursday';
                return [
                    'Utorak'=>$datum->subDays(2)->toDateString(),
                    'Sreda'=>$datum->addDays(1)->toDateString(),
                    'Četvrtak'=>date('Y-m-d')
                ];
            case 'Friday';
                return [
                    'Petak'=>date('Y-m-d'),
                    'Subota'=>$datum->addDays(1)->toDateString(),
                    'Nedelja'=>$datum->addDays(1)->toDateString(),
                    'Ponedeljak'=>$datum->addDays(1)->toDateString()
                ];
            case 'Saturday';
                return [
                    'Petak'=>$datum->subDays(1)->toDateString(),
                    'Subota'=>date('Y-m-d'),
                    'Nedelja'=>$datum->addDays(2)->toDateString(),
                    'Ponedeljak'=>$datum->addDays(1)->toDateString()
                ];

            default:
                return [
                    'Petak'=>$datum->subDays(2)->toDateString(),
                    'Subota'=>$datum->addDays(1)->toDateString(),
                    'Nedelja'=>date('Y-m-d'),
                    'Ponedeljak'=>$datum->addDays(2)->toDateString()
                ];
        }
    }
    /*CRTANJE UCINKA*/
    private function drawScore($id){
        $tips = Tip::where('user_id',$id)->get();
        $w = 0; $l = 0; $v = 0;
        foreach ($tips as $tip){
            switch($tip->status){
                case 1:
                    $w++;
                    break;
                case 2:
                    $l++;
                    break;
                case 3:
                    $v++;
                    break;
                default:
                    false;
            }
        }
        return $w.'-'.$l.'-'.$v;
    }

    public function posaljiMejl($id){
        $usr = User::find($id);
        Mail::to($usr->email)->send(new Register($usr));
        return view ('front.info.regmail');
    }

    public function registracija(Request $request){
        $usr = User::create([
            'username' => $request['username'],
            'firstname' => '',
            'lastname' => '',
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'role'=>'U',
            'avatar'=>'user.png',
            'email_code'=> bcrypt(rand(111111,999999))
        ]);
        UserDetail::create(['user_id'=>$usr->id]);
        UserStat::create(['user_id'=>$usr->id]);

        return redirect('/registracija/posalji-mejl/'.$usr->id);
    }

    public function potvrda($id,$code){
        $user = User::find($id);
        if($user->email_code === $code) {
            $user->status = 1;
            $user->save();
            return view('front.info.dareg');
        }
        else {
            return view('.front.info.nereg');
        }


    }


}