<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Article;
use App\Ninca;
use App\Ticket;
use App\TicketDetail;
use App\Tip;
use App\TipBet;
use App\TipGame;
use App\TipOdd;
use App\TipPar;
use App\TipText;
use App\Sport;
use App\Location;
use App\ArticleComment;
use App\TipComment;
use App\User;
use App\UserStat;
use App\Mail\Register;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    public function blog()
    {
        $blogs = Article::orderBy('created_at','desc')->paginate(10);
        return view('admin.blog.blog',compact('blogs'));

    }
    public function blogNovi()
    {
        return view('admin.blog.new');
    }
    public function blogNew(Request $request){
        $file = Input::file('image');
        $filename=rand(1111,9999).$_FILES['image']['name'];
        $file->move('images/blog/',$filename);
        Image::make('images/blog/'.$filename)->resize(900,650)->save('images/blog/'.$filename);
        Article::create([
            'user_id'=>\Auth::user()->id,
            'title'=>$request['title'],
            'slug'=>str_slug($request['title']),
            'excerpt'=>$request['excerpt'],
            'content'=>$request['content'],
            'image'=>$filename
        ]);
        return redirect('admin/blog/svi');
    }
    public function blogIzmeni($id)
    {
        $blog = Article::find($id);
        return view('admin.blog.izmeni', compact('blog'));
    }
    public function blogEdit(Request $request)
    {
        $blog = Article::find($request['blogid']);
        if(Input::hasFile('image')){
            $file = Input::file('image');
            $filename=rand(1111,9999).$_FILES['image']['name'];
            $file->move('images/blog/',$filename);
            Image::make('images/blog/'.$filename)->resize(900,650)->save('images/blog/'.$filename);
            unlink('images/blog/'.$blog->image);
        }else{
            $filename = $blog->image;
        }
        $blog->title = $request['title'];
        $blog->slug = str_slug($request['title']);
        $blog->excerpt = $request['excerpt'];
        $blog->content = $request['content'];
        $blog->image = $filename;
        $blog->save();

        return redirect('admin/blog/izmeni/'.$request['blogid']);
    }
    public function blogDel($id){
        $blog = Article::find($id);
        $blog->delete();
        return redirect('admin/blog/svi');
    }
    public function blogKomentari()
    {
        $coms = ArticleComment::orderBy('created_at', 'desc')->paginate(15);
        return view('admin.blog.komentari', compact('coms'));
    }
    public function blogNinca()
    {
        $blog=Ninca::first();
        return view('admin.blog.ninca', compact('blog'));
    }
    public function blogNincanew(Request $request)
    {
        $images = '';
        $files = Input::file('images');
        foreach($files as $file){
            $filename = rand(11111111,99999999).'.jpg';
            $file->move('images/blog/',$filename);
            $images .= $filename.';';
        }
        rtrim($images,';');
        Ninca::create([
            'title'=>$request['title'],
            'slug'=>str_slug($request['title']),
            'excerpt'=>$request['excerpt'],
            'content'=>$request['content'],
            'image'=>$images
        ]);
        return redirect('admin/blog/svi');

    }
    public function comAcc($id)
    {
        $com = ArticleComment::find($id);
        $com->status = 1;
        $com->save();
        return redirect('admin/blog/komentari');
    }
    public function comDel($id)
    {
        $com = ArticleComment::find($id);
        $com->delete();
        return redirect('admin/blog/komentari');
    }
    public function tiketi()
    {
        $tiketi=Ticket::orderBy('created_at','desc')->paginate(10);
        return view('admin.tiketi.index', compact('tiketi'));
    }
    public function tiketiAdd(Request $request)
    {
        $num = rand(11111111,99999999);
        $br = $request['broj_parova'];
        $kvota = 1;
        if(isset($request['pocetna'])){
            DB::table('tickets')->update(['pocetna'=>0]);
            $poc = 1;
        }else{
            $poc = 0;
        }
        $tik = Ticket::create([
            'number'=>$num,
            'tip'=>$br,
            'vrsta'=>$request['vrsta'],
            'datum'=>date('Y-m-d',strtotime($request['datum_prikaz'])),
            'kvota'=>$kvota,
            'verovatnoca'=>$request['verovatnoca'],
            'kladionica'=>$request['kladionica'],
            'uplata'=>0,
            'pocetna'=>$poc,
            'status'=>0
        ]);
        for($i=1;$i<=$br;$i++){
            $kvota *= $request['kvota'.$i];
            TicketDetail::create([
                'ticket_id'=>$tik->id,
                'sport'=>$request['sport'.$i],
                'datum'=>date('Y-m-d',strtotime($request['datum'.$i])),
                'vreme'=>$request['vreme'.$i].':00',
                'par_home'=>$request['par_home'.$i],
                'par_away'=>$request['par_away'.$i],
                'tip'=>$request['tip'.$i],
                'kvota'=>$request['kvota'.$i]

            ]);
        }
        $tik->kvota = $kvota;
        $tik->save();
        return redirect('/admin/tiketi/svi');
    }
    public function tiketiDel($id){
        Ticket::destroy($id);
        return redirect('admin/tiketi/svi');
    }
    public function tiketiHome($id){
        DB::table('tickets')->update(['pocetna'=>0]);
        $tik = Ticket::find($id);
        $tik->pocetna = 1;
        $tik->save();
        return redirect('/admin/tiketi/svi');
    }
    public function analize()
    {
        $tips = Tip::where('datum','>',date('Y-m-d H:i:s'))->orderBy('datum')->paginate(10);
        $olds = Tip::where('datum','<=',date('Y-m-d H:i:s'))->orderBy('datum')->paginate(10);
        return view('admin.analize.index',compact('tips','olds'));
    }
    public function analizeNew(){
        return view('admin.analize.new');
    }
    public function analizeAdd(Request $request){
        if(Input::hasFile('tiket')){
            $file = Input::file('tiket');
            $tiketname = rand(1111, 9999).preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['tiket']['name']);
            $file->move('images/screenshots/',$tiketname);
            Image::make('images/screenshots/'.$tiketname)->resize(1000,600)->save('images/screenshots/'.$tiketname);
        }else{
            $tiketname = '';
        }
        $tip = Tip::create([
            'user_id'=>\Auth::user()->id,
            'slug'=>str_slug($request['home'].' '.$request['away']),
            'datum'=>date('Y-m-d',strtotime($request['datum'])).' '.$request['vreme'].':00',
            'vreme'=>'00:00:00'
        ]);
        TipBet::create([
            'tip_id'=>$tip->id,
            'value'=>$request['ulog']
        ]);
        TipText::create([
            'tip_id'=>$tip->id,
            'description'=>$request['content'],
            'tiket'=>$tiketname,
            'lang'=>$request['lang']
        ]);
        TipPar::create([
            'tip_id'=>$tip->id,
            'home'=>$request['home'],
            'away'=>$request['away']
        ]);
        TipOdd::create([
            'tip_id'=>$tip->id,
            'value'=>$request['kvota'],
            'location'=>$request['kladionica']
        ]);
        TipGame::create([
            'tip_id'=>$tip->id,
            'name'=>$request['tip']
        ]);
        DB::table('sport_tip')->insert([
            'sport_id'=>$request['sport'],
            'tip_id'=>$tip->id
        ]);

        return redirect('admin/analize/sve');
    }
    public function analKom()
    {
        $coms = TipComment::orderBy('created_at', 'desc')->paginate(15);
        return view('admin.analize.komentari', compact('coms'));
    }
    public function acomAcc($id)
    {
        $com = TipComment::find($id);
        $com->status = 1;
        $com->save();
        return redirect('admin/analize/komentari');
    }
    public function acomDel($id)
    {
        $com = TipComment::find($id);
        $com->delete();
        return redirect('admin/analize/komentari');
    }
    public function analizeResult(Request $request){
        $tip = Tip::find($request['anid']);
        $tip->status = $request['status'];
        $tip->save();
        $par = $tip->pars;
        $par->period1 = $request['period1'];
        $par->period2 = $request['period2'];
        $par->period3 = $request['period3'];
        $par->period4 = $request['period4'];
        $par->period5 = $request['period5'];
        $par->result = $request['result'];
        $par->save();
        $this->calcStats($tip->users->id);
        return redirect('admin/analize/sve');
    }
    public function setProfil()
    {
        return view('admin/setings/index');
    }
    public function adminEditpic(Request $request)
    {
        $file = Input::file('slika');
        $filename = rand(1111, 9999).preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['slika']['name']);
        $file->move('images/avatars/', $filename);
        Image::make('images/avatars/'.$filename)->resize(360,360)->save('images/avatars/'.$filename);
        $usr = User::find(\Auth::user()->id);
        $usr->avatar = $filename;
        $usr->save();
        return redirect('/admin/podesavanja/profil');
    }
    public function addAdmin()
    {
        if(\Auth::user()->id==3) {
            $usrs = User::orderBy('created_at', 'desc')->paginate(20);
            return view('admin.setings.addadmin', compact('usrs'));
        }else{
            return view('errors.202');
        }
    }
    public function adminAdd($id)
    {
        $usr = User::find($id);
        $usr->role = 'A';
        $usr->save();
        return redirect('/admin/podesavanja/dodaj-admina');
    }
    public function adminDel($id)
    {
        $usr = User::find($id);
        $usr->role = 'U';
        $usr->save();
        return redirect('/admin/podesavanja/dodaj-admina');
    }
    public function sportKlad()
    {
        $sps = Sport::all();
        $klads = Location::all();
        return view('admin.setings.sptkld', compact('sps', 'klads'));
    }
    public function addKlad(Request $request)
    {
        $file = Input::file('slika');
        $filename = rand(1111, 9999).preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['slika']['name']);
        $file->move('images/kladionice/', $filename);
        Location::create([
            'name'=>$request['ime'],
            'icon'=>$filename,
            'url'=>$request['link']
        ]);
        return redirect('admin/podesavanja/sport-klad');
    }
    public function addSport(Request $request)
    {
        $file = Input::file('slika');
        $filename = rand(1111, 9999).preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['slika']['name']);
        $file->move('images/sports/', $filename);
        Sport::create([
            'name'=>$request['ime'],
            'icon'=>$filename
        ]);
        return redirect('admin/podesavanja/sport-klad');
    }
    public function dellKlad($id)
    {
        $kld = Location::find($id);
        $kld->delete();
        return redirect('/admin/podesavanja/sport-klad');
    }
    public function dellSport($id)
    {
        $kld = Sport::find($id);
        $kld->delete();
        return redirect('/admin/podesavanja/sport-klad');
    }


    /*racunanje statistike korisnika*/
    private function calcStats($id){
        $ulog = 0;$profit = 0;
        $ultips = Tip::where('user_id',$id)->where('status','<>',0)->get();
        $uktips = count($ultips);
        foreach($ultips as $ultip){
            if($ultip->status==1) {
                $ulog += $ultip->bets->value;
                $profit += $ultip->bets->value*$ultip->odds->value;
            }elseif($ultip->status==2){
                $ulog += $ultip->bets->value;
                $profit -= $ultip->bets->value;
            }
        }
        $uspeh = ($profit/$ulog-1)*100;

        $usr = User::find($id)->stats;
        $usr->broj = $uktips;
        $usr->ulog = $ulog;
        $usr->profit = $profit;
        $usr->procenat = $uspeh;
        $usr->save();
    }



}