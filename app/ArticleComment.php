<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleComment extends Model
{
    protected $fillable = ['article_id','user_id','text','status'];

    public function articles(){
        return $this->belongsTo('App\Article','article_id');
    }
    public function users(){
        return $this->belongsTo('App\User','user_id');
    }
}
