<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    protected  $fillable = ['user_id','slug','datum','vreme','status'];

    public function users(){
        return $this->belongsTo('App\User','user_id');
    }
    public function bets(){
        return $this->hasOne('App\TipBet');
    }
    public function games(){
        return $this->hasOne('App\TipGame');
    }
    public function odds(){
        return $this->hasOne('App\TipOdd');
    }
    public function pars(){
        return $this->hasOne('App\TipPar');
    }
    public function texts(){
        return $this->hasOne
        ('App\TipText');
    }
    public function sports(){
        return $this->belongsToMany('App\Sport');
    }
    public function icons(){
        return $this->hasOne('App\Icon','status');
    }
    public function tipcoms(){
        return $this->hasMany('App\TipComment');
    }
}
